"use strict";

const
  DicXmlValue = require('./DicXmlValue'),
  { has, get, isEqual, keys } = require('lodash'),
  { DicXmlParams } = require('dim-xml');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('dim-xml').DicXmlParams.XmlParamPart} XmlParamPart
 * @typedef {import('@node-red/registry').NodeMessageInFlow & {
 *  error: number,
 *  status: number,
 *  message: string
 * }} AckMsg
 *
 * @typedef {import('@node-red/registry').NodeMessageInFlow & {
 *  timestamp: number?
 * }} StampedMsg
 *
 * @typedef {nodeRedDimXml.DimXmlClientDef &
 *  nodeRedDimXml.DicValueOpts} DicXmlParamsDef
 * @typedef {nodeRedDimXml.DimXmlClientNode &
 *  nodeRedDimXml.DicValueOpts} DicXmlParamsNode
 * @typedef {XmlParamPart | Array<XmlParamPart>} Params
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {
  /**
   * @brief constructor function of DIM-XML Params Client Node
   * @param {DicXmlParamsDef} c
   * @this {DicXmlParamsNode & {client: DicXmlParams}}
   */
  function DimXmlParamsClient(c) {
    DicXmlValue.call(this, RED, c, DicXmlParams, "dicXmlParams");
    const node = this;

    if (node.dnsClient) {
      if (!(node.client instanceof DicXmlParams)) {
        node.client = new DicXmlParams(node.serviceName, null,  node.dnsClient);
      }

      // input handlers cleanup
      node.removeAllListeners('input');

      // handle input commands and click events
      node.on('input', function(msg, send, done) {

        if (isEqual(keys(msg), [ '_msgid' ])) { // triggered by clicking button
          DicXmlParams.get(node.serviceName + '/Aqn', node.dnsClient)
          .then((/** @type {any} */val) => {

            if (node.client && node.stamped) {
              /** @type {StampedMsg} */(msg)
              .timestamp = get(node.client, 'timestamp');
            }
            msg.payload = val;
            node.send(msg);
            done();
          })
          .catch(done);
        }
        else if (msg.hasOwnProperty('payload')) { // commands

          node.client.setParams(/** @type {Params} */(msg.payload))
          .then(() => { done(); })
          .catch((err) => {
            if (has(err, 'key')) {
              /** @type {AckMsg} */(msg).error = err.error;
              /** @type {AckMsg} */(msg).status = err.status;
              /** @type {AckMsg} */(msg).message = get(err, 'message', '');
              msg.payload = [];
              send(msg);
              done();
            }
            else { done(err); } /* command rejected */
          });
        }
        else {
          done();
        }
      });
    }
  }
  RED.nodes.registerType("dim-xml-params-client", DimXmlParamsClient);
};
