"use strict";

const
  DicXmlValue = require('./DicXmlValue'),
  { DicXmlDataSet } = require('dim-xml');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 *
 * @typedef {nodeRedDimXml.DimXmlClientDef &
 *  nodeRedDimXml.DicValueOpts} DicXmlDataSetDef
 * @typedef {nodeRedDimXml.DimXmlClientNode &
 *  nodeRedDimXml.DicValueOpts} DicXmlDataSetNode
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {
  /**
   * @brief constructor function of DIM-XML DataSet Client Node
   * @param {DicXmlDataSetDef} c
   * @this {DicXmlDataSetNode}
   */
  function DimXmlDataSetClient(c) {
    DicXmlValue.call(this, RED, c, DicXmlDataSet, "dicXmlDataSet");
  }
  RED.nodes.registerType("dim-xml-dataset-client", DimXmlDataSetClient);
};
