"use strict";

const
  DicXmlValue = require('./DicXmlValue'),
  { DicXmlState } = require('dim-xml');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 *
 * @typedef {nodeRedDimXml.DimXmlClientDef &
 *  nodeRedDimXml.DicValueOpts} DicXmlStateDef
 * @typedef {nodeRedDimXml.DimXmlClientNode &
 *  nodeRedDimXml.DicValueOpts} DicXmlStateNode
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {
  /**
   * @brief constructor function of DIM-XML State Client Node
   * @param {DicXmlStateDef} c
   * @this {DicXmlStateNode}
   */
  function DimXmlStateClient(c) {
    DicXmlValue.call(this, RED, c, DicXmlState, "dicXmlState");
  }
  RED.nodes.registerType("dim-xml-state-client", DimXmlStateClient);
};
