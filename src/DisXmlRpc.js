"use strict";

const
  { xml } = require('dim-xml'),
  TIMEOUT = require('dim-xml').DicXmlCmd.TIMEOUT,
  { makeDeferred, timeout } = require('prom'),
  { invoke, omit, get, defaultTo, toString } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimXmlConf').ConfNode} ConfNode
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM-XML RPC In Node
   * @param {nodeRedDimXml.DimXmlServerDef} c
   * @this {nodeRedDimXml.DimXmlServerNode}
   */
  function DimXmlRpcIn(c) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);
      node.disXmlNode = node.cnf.getDisXmlNode();
      /** @type {prom.Deferred?} */

      if (node.disXmlNode) {
        node.cnf.ref();
        const rpcSrv = node.disXmlNode.addXmlRpc(node.serviceName,
          async (req, ret) => {
            const d = makeDeferred();
            const params = xml.toJs(req.xml);

            /** @type {{
             * _msgid: string, key: string,
             * payload: unknown, res?: prom.Deferred
             * }}
             */
            var msg = {
              _msgid: RED.util.generateId(),
              key: get(params['$'], 'key', ""),
              payload: omit(params, [ '$' ]), /* parameters */
              res: d
            };
            const ack = timeout(d.promise, defaultTo(req.timeout, TIMEOUT));
            node.send(msg);
            xml.fromJs(ret, await ack); /* set returns */
          }
        );

        if (!rpcSrv) {
          invoke(node, '_removeConnStatus');
          if (invoke(node.disXmlNode, 'isService', node.serviceName + '/RpcIn') ||
            invoke(node.disXmlNode, 'isService', node.serviceName + '/RpcOut')) {
            node.error(RED._("disXmlRpc.error.dup-service",
              { service: node.serviceName }));
          }
          else {
            node.error(RED._("disXmlRpc.error.service-reg-failed",
              { service: node.serviceName }));
          }
          node.disXmlNode = null;
        }

        node.on('close', function(/** @type {() => void} */ done) {
          invoke(node.disXmlNode, 'removeXmlRpc', node.serviceName);
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.disXmlNode = null;
          done();
        });
      }
    }
    else {
      node.error(RED._("node-red-dim-xml/dimXmlConf:dimXmlConf.error.config-missing"));
    }
  }
  RED.nodes.registerType("dim-xml-rpc in", DimXmlRpcIn);


  /**
   * @brief constructor function of DIM-XML Rpc Response Node
   * @param {nodeRedDimXml.DimXmlServerDef} c
   * @this {nodeRedDimXml.DimXmlServerNode}
   */
  function DimXmlRpcRes(c) {
    RED.nodes.createNode(this, c);
    const node = this;

    node.on('input', function(msg, send, done) {
      if (msg.hasOwnProperty("payload")) {
        // @ts-ignore: 'res' checked in the first condition
        if ((get(msg, 'res.promise') instanceof Promise) && msg.res.isPending) {
          try {
            const error = get(msg, 'error');

            if (Number.isInteger(error)) {
              const errMsg = toString(msg.payload);
              // @ts-ignore: 'res' checked above
              msg.res.reject({ message: errMsg, error: error });
              node.debug(RED._("disXmlRpc.error.request-failed") + ": " +
                errMsg);
            }
            else {
              // @ts-ignore: 'res' checked above
              msg.res.resolve(msg.payload);
              node.debug(RED._("disXmlRpc.success.request-served"));
            }
            done();
          }
          catch (err) {
            done(err);
          }
        }
        else {
          done(new Error(RED._("disXmlRpc.error.request-failed")));
        }
      }
      else {
        done();
      }
    });
  }
  RED.nodes.registerType("dim-xml-rpc-response", DimXmlRpcRes);
};
