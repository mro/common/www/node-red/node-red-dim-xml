const
  DimXmlConf = require('./DimXmlConf'),
  DicXmlCmd = require('./DicXmlCmd'),
  DisXmlCmd = require('./DisXmlCmd'),
  DicXmlRpc = require('./DicXmlRpc'),
  DisXmlRpc = require('./DisXmlRpc'),
  DicXmlState = require('./DicXmlState'),
  DisXmlState = require('./DisXmlState'),
  DicXmlDataSet = require('./DicXmlDataSet'),
  DisXmlDataSet = require('./DisXmlDataSet'),
  DicXmlParams = require('./DicXmlParams'),
  DisXmlParams = require('./DisXmlParams');

module.exports = { DimXmlConf, DicXmlCmd, DisXmlCmd, DicXmlRpc, DisXmlRpc,
  DicXmlState, DisXmlState, DicXmlDataSet, DisXmlDataSet, DicXmlParams,
  DisXmlParams };
