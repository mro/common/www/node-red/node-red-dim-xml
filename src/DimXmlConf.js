"use strict";

const
  { transform } = require('lodash'),

  { DisXmlNode } = require('dim-xml'),
  { DnsClient, DicDns, ServiceDefinition } = require('dim');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('@node-red/registry').Node} Node
 * @typedef {import('@node-red/registry').NodeDef} NodeDef
 * @typedef {import('dim').packets.DnsDupInfo} DnsDupInfo
 *
 * @typedef {{ name: string, definition: string, type: string }[]} ServiceList
 *
 * @typedef {Node & ExtraProps} ConfNode
 * @typedef {{
 *  _host: string,
 *  _port: number,
 *  _retry: number,
 *  _connected: boolean | null,
 *  _dnsClient: DnsClient | null,
 *  _disXmlNode: DisXmlNode | null,
 *  _ref: number,
 *  _close(): void,
 *  _onDnsConnect(): void,
 *  _onDnsDisconnect(): void,
 *  _onDupService(info: string[]): void,
 *  _onDupTaskName(info: DnsDupInfo): void,
 *  _onExitCmd(cmdInfo: number): void,
 *  ref(): void,
 *  unref(): void,
 *  getDnsClient(): DnsClient,
 *  getDisXmlNode(): DisXmlNode,
 *  addConnStatus(cli: Node): (() => void) | null
 * }} ExtraProps
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM Config Node
   * @param {NodeDef & { host: string, port: number, retry: number }} c
   * @this {Node & ConfNode}
   */
  function DimXmlConf(c) {
    RED.nodes.createNode(this, c);
    const node = this;

    node._host = c.host;
    node._port = c.port;
    node._retry = (c.retry < 1000) ? 1000 : c.retry;
    node._connected = null;

    node._dnsClient = null;
    node._disXmlNode = null;
    node._ref = 0;

    node.getDnsClient = function() {
      if (!node._dnsClient) {
        node._dnsClient = new DnsClient(node._host, node._port,
          { retryDelay: node._retry });

        node._onDnsConnect = () => { node._connected = true; };
        node._dnsClient.on('connect', node._onDnsConnect);

        node._onDnsDisconnect = () => { node._connected = false; };
        node._dnsClient.on('disconnect', node._onDnsDisconnect);
      }
      return node._dnsClient;
    };

    node.getDisXmlNode = function() {
      if (!node._disXmlNode) {
        node._disXmlNode = new DisXmlNode();

        node._onDupService = (info) => {
          node.error(RED._("dimXmlConf.error.dup-service",
            { info: info.toString() }));
        };
        node._disXmlNode.on('duplicate:services', node._onDupService);

        node._onDupTaskName = (info) => {
          node.error(RED._("dimXmlConf.error.dup-taskName",
            { info: info.task }));
        };
        node._disXmlNode.on('duplicate:taskName', node._onDupTaskName);

        node._onExitCmd = (cmdInfo) => {
          node.warn(RED._("dimXmlConf.error.exit-command",
            { info: cmdInfo }));
        };
        node._disXmlNode.once('exit', node._onExitCmd);

        node._disXmlNode.register(node.getDnsClient())
        .catch((err) => node.error(err));
      }
      return node._disXmlNode;
    };

    node.ref = () => {
      if (node._ref++ < 0) {
        node.error(RED._("dimXmlConf.error.ref-count"));
      }
    };

    node.unref = () => {
      if (--node._ref === 0) {
        node._close();
      }
    };

    node.addConnStatus = function(/** @type {Node} */ cli) {
      if (cli) {
        /* Check if the connection status is already known */
        if (node._connected === true) {
          cli.status({ fill: "green", shape: "dot",
            text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" });
        }
        else if (node._connected === false) {
          cli.status({ fill: "red", shape: "ring",
            text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.disconnected" });
        }
        else {
          cli.status({ fill: "yellow", shape: "ring",
            text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connecting" });
        }

        const onConnect = () => cli.status(
          { fill: "green", shape: "dot",
            text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" });
        const onDisconnect = () => cli.status(
          { fill: "red", shape: "ring",
            text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.disconnected" });
        const dnsClient = node.getDnsClient();

        dnsClient.on('connect', onConnect);
        dnsClient.on('disconnect', onDisconnect);
        return () => {
          dnsClient.removeListener('connect', onConnect);
          dnsClient.removeListener('disconnect', onDisconnect);
          cli.status({});
        };
      }
      return null;
    };

    node._close = function() {
      if (node._disXmlNode) {
        node._disXmlNode.removeListener('duplicate:services',
          node._onDupService);
        node._disXmlNode.removeListener('duplicate:taskName',
          node._onDupTaskName);
        node._disXmlNode.removeListener('exit', node._onExitCmd);
        node._disXmlNode.close();
      }
      if (node._dnsClient) {
        node._dnsClient.removeListener('connect', node._onDnsConnect);
        node._dnsClient.removeListener('disconnect', node._onDnsDisconnect);
        node._dnsClient.unref();
      }
      node._disXmlNode = null;
      node._dnsClient = null;
    };

    node.on('close', (/** @type {() => void} */ done) => {
      node._close();
      done();
    });
  }
  RED.nodes.registerType("dim-xml-conf", DimXmlConf);

  RED.httpAdmin
  .get("/service-list/:server", function(req, res) {
    if (req.params.server) {
      DicDns.serviceInfo('*', 'tcp://' + req.params.server)
      .then((list) => {
        /** @type {ServiceList} */
        const srvList = transform(list,
          /** @type {function(ServiceList, ServiceDefinition, string): void} */
          function(ret, srvDef, srvName) {
            let def = '', t = '';
            if (srvDef instanceof ServiceDefinition) {
              def = ServiceDefinition.toStringParams(srvDef.params);
              switch (srvDef.type) {
              case ServiceDefinition.Type.RPC:
                def += ',' + ServiceDefinition.toStringParams(srvDef.returns);
                t = 'RPC'; break;
              case ServiceDefinition.Type.CMD:
                t = 'CMD'; break;
              case ServiceDefinition.Type.SRV:
                t = 'SRV'; break;
              default:
              }
            }
            ret.push({ name: srvName, definition: def, type: t });
          }, []);
        res.status(200).json({ list: srvList });
      })
      .catch((err) => res.status(500).send(err));
    }
    else {
      res.sendStatus(404);
    }
  });
};
