"use strict";

const
  { DicXmlDataSet } = require('dim-xml'),
  { invoke, isNil } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimXmlConf').ConfNode} ConfNode
 * @typedef {import('dim-xml').XmlDataSet} XmlDataSet
 * @typedef {import('dim-xml').DisXmlDataSet.UpdatePart} UpdatePart
 *
 * @typedef {nodeRedDimXml.DimXmlServerDef & ExtraProps} DisXmlDataSetDef
 * @typedef {nodeRedDimXml.DimXmlServerNode & ExtraProps} DisXmlDataSetNode
 * @typedef {{ dataset: XmlDataSet }} ExtraProps
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {
  /**
   * @brief constructor function of DIM-XML DataSet Server Node
   * @param {DisXmlDataSetDef} c
   * @this {DisXmlDataSetNode}
   */
  function DisXmlDataSet(c) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    this.dataset = c.dataset;
    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);
      node.disXmlNode = node.cnf.getDisXmlNode();
      if (node.disXmlNode) {
        node.cnf.ref();
        const srv = node.disXmlNode.addXmlDataSet(node.serviceName);
        const up = (/** @type {any} */val) => {
          const dataset = DicXmlDataSet.parse(val || '');
          if (isNil(dataset)) {
            node.error(RED._("disXmlDataSet.error.updated-value"));
          }
          else {
            node.send({ _msgid: RED.util.generateId(), payload: dataset });
          }
        };

        if (!srv) {
          invoke(node, '_removeConnStatus');
          if (invoke(node.disXmlNode, 'isService', node.serviceName)) {
            node.error(RED._("disXmlDataSet.error.dup-service",
              { service: node.serviceName }));
          }
          else {
            node.error(RED._("disXmlDataSet.error.service-reg-failed",
              { service: node.serviceName }));
          }
          node.disXmlNode = null;
        }
        else {
          srv.on('update', up);

          /* initialize the DataSet service */
          node.dataset.forEach((data) => srv.add(data));

          node.on('input', function(msg, send, done) {
            if (msg.hasOwnProperty("payload")) {
              srv.update(/** @type {UpdatePart}*/(msg.payload));
            }
            done();
          });
        }

        node.on('close', function(/** @type {() => void} */ done) {
          invoke(srv, 'removeListener', 'update', up);
          invoke(node.disXmlNode, 'removeXmlDataSet', node.serviceName);
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.disXmlNode = null;
          done();
        });
      }
    }
    else {
      node.error(RED._("node-red-dim-xml/dimXmlConf:dimXmlConf.error.config-missing"));
    }
  }
  RED.nodes.registerType("dim-xml-dataset-server", DisXmlDataSet);
};
