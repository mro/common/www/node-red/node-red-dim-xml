import { Node, NodeDef } from '@node-red/registry';
import { DnsClient } from 'dim';
import { DisXmlNode } from 'dim-xml';
import { ConfNode } from './DimXmlConf';

export = nodeRedDimXml;
export as namespace nodeRedDimXml;

declare namespace nodeRedDimXml {
  interface DimXmlNodeDef extends NodeDef {
    cnf: string; /* node id */
    serviceName: string;
  }
  type DimXmlClientDef = DimXmlNodeDef & { timeout: number };
  type DimXmlServerDef = DimXmlNodeDef;

  interface DimXmlNode extends Node {
    cnf: ConfNode;
    serviceName: string;

    _removeConnStatus?: (() => void) | null;
  }

  type DimXmlClientNode = DimXmlNode & {
    timeout: number;
    dnsClient: DnsClient | null;
  };
  type DimXmlServerNode = DimXmlNode & {
    disXmlNode: DisXmlNode | null;
  };

  type DicValueOpts = {
    monitored: boolean,
    scheduled: boolean,
    stamped: boolean
  }
}
