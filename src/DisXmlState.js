"use strict";

const
  { DicXmlState } = require('dim-xml'),
  { invoke, get, isInteger, isArray, toNumber,
    isNil } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimXmlConf').ConfNode} ConfNode
 *
 * @typedef {nodeRedDimXml.DimXmlServerDef & ExtraProps} DisXmlStateDef
 * @typedef {nodeRedDimXml.DimXmlServerNode & ExtraProps} DisXmlStateNode
 * @typedef {{
 *  initState: string,
 *  states: Array<{ value: number, name: string }>,
 * }} ExtraProps
 *
 * @typedef {{ code: number | string, message?: string }} info
 * @typedef {{
 *  state?: number | string,
 *  error?: info,
 *  errors?: Array<info>,
 *  warning?: info,
 *  warnings?: Array<info>
 * }} inputValue
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM-XML State Server Node
   * @param {DisXmlStateDef} c
   * @this {DisXmlStateNode}
   */
  function DisXmlState(c) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    this.initState = c.initState;
    this.states = c.states;
    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);
      node.disXmlNode = node.cnf.getDisXmlNode();
      if (node.disXmlNode) {
        node.cnf.ref();
        const srv = node.disXmlNode.addXmlState(node.serviceName);
        const up = (/** @type {any} */val) => {
          const state = DicXmlState.parse(val || '');
          if (isNil(state)) {
            node.error(RED._("disXmlState.error.updated-value"));
          }
          else {
            node.send({ _msgid: RED.util.generateId(), payload: state });
          }
        };

        if (!srv) {
          invoke(node, '_removeConnStatus');
          if (invoke(node.disXmlNode, 'isService', node.serviceName)) {
            node.error(RED._("disXmlState.error.dup-service",
              { service: node.serviceName }));
          }
          else {
            node.error(RED._("disXmlState.error.service-reg-failed",
              { service: node.serviceName }));
          }
          node.disXmlNode = null;
        }
        else {
          srv.on('update', up);

          /* initialize the State service */
          node.states.forEach((st) => {
            const value = toNumber(st.value);
            if (!isInteger(value) ||
              !srv.addState(value, st.name)) {
              node.error(RED._("disXmlState.error.add-state",
                { state: st.value }));
            }
          });

          const initState = toNumber(node.initState);
          if (!isInteger(initState) || !srv.setState(initState)) {
            node.error(RED._("disXmlState.error.set-state",
              { state: node.initState }));
          }

          // eslint-disable-next-line complexity, max-statements
          node.on('input', function(msg, send, done) {
            if (msg.hasOwnProperty("payload")) {

              const value = /** @type {inputValue} */(msg.payload);
              if (!isNil(value.state)) {
                const state = toNumber(value.state);
                if (!isInteger(state) || !srv.setState(state)) {
                  node.error(RED._("disXmlState.error.set-state",
                    { state: state }), msg);
                }
              }

              /* errors */
              if (value.error) {
                const code = toNumber(value.error.code);
                if (isInteger(code)) {
                  srv.addError(code, get(value.error, 'message', ''));
                }
                else {
                  node.error(RED._("disXmlState.error.add-error",
                    { code: value.error.code }), msg);
                }
              }

              if (isArray(value.errors)) {
                const oldErrs = srv.states.errors;
                srv.states.errors = []; /* reset errors */

                for (const error of value.errors) {
                  const code = toNumber(error.code);
                  if (isInteger(code)) {
                    srv.addError(code, get(error, 'message', ''));
                  }
                  else {
                    node.error(RED._("disXmlState.error.add-error",
                      { code: code }), msg);
                    srv.states.errors = oldErrs; /* restore errors */
                    break;
                  }
                }
              }

              /* warnings */
              if (value.warning) {
                const code = toNumber(value.warning.code);
                if (isInteger(code)) {
                  srv.addWarning(code, get(value.warning, 'message', ''));
                }
                else {
                  node.error(RED._("disXmlState.error.add-warning",
                    { code: value.warning.code }), msg);
                }
              }

              if (isArray(value.warnings)) {
                const oldWarns = srv.states.warnings;
                srv.states.warnings = []; /* reset warnings */

                for (const warning of value.warnings) {
                  const code = toNumber(warning.code);
                  if (isInteger(code)) {
                    srv.addWarning(code, get(warning, 'message', ''));
                  }
                  else {
                    node.error(RED._("disXmlState.error.add-warning",
                      { code: code }), msg);
                    srv.states.warnings = oldWarns; /* restore warnings */
                    break;
                  }
                }
              }
            }
            done();
          });
        }

        node.on('close', function(/** @type {() => void} */ done) {
          invoke(srv, 'removeListener', 'update', up);
          invoke(node.disXmlNode, 'removeXmlState', node.serviceName);
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.disXmlNode = null;
          done();
        });
      }
    }
    else {
      node.error(RED._("node-red-dim-xml/dimXmlConf:dimXmlConf.error.config-missing"));
    }
  }
  RED.nodes.registerType("dim-xml-state-server", DisXmlState);
};
