"use strict";

const
  { DicXmlCmd, xml } = require('dim-xml'),
  { invoke, get, has } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimXmlConf').ConfNode} ConfNode
 *
 * @typedef {import('@node-red/registry').NodeMessageInFlow & {
 *  key: number,
 *  error: number,
 *  status: number
 * }} AckMsg
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM-XML CMD Client Node
   * @param {nodeRedDimXml.DimXmlClientDef} c
   * @this {nodeRedDimXml.DimXmlClientNode}
   */
  function DimXmlCmdClient(c) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    this.timeout = c.timeout; /* ms */

    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);
      node.dnsClient = node.cnf.getDnsClient();
      if (node.dnsClient) {
        node.cnf.ref();
        /* query service to check connection status */
        node.dnsClient.query(node.serviceName + '/Cmd')
        .catch((err) => {
          node.error(err);
        });

        node.on('input', function(msg, send, done) {
          if (msg.hasOwnProperty("payload")) {
            DicXmlCmd.invoke(node.serviceName, msg.payload, node.dnsClient,
              null, node.timeout)
            .catch((err) => {
              if (!has(err, 'key')) { throw err; }
              else { return err; } /* command rejected */
            })
            .then((rep) => {
              /** @type {AckMsg} */(msg).key = get(rep, 'key');
              /** @type {AckMsg} */(msg).error = rep.error;
              /** @type {AckMsg} */(msg).status = rep.status;
              msg.payload = get(rep, 'message') || xml.getText(rep.xml);
              send(msg);
              done();
            }).catch(done);
          }
          else {
            done();
          }
        });

        node.on('close', (/** @type {() => void} */ done) => {
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.dnsClient = null;
          done();
        });
      }
    }
    else {
      node.error(RED._("node-red-dim-xml/dimXmlConf:dimXmlConf.error.config-missing"));
    }

  }
  RED.nodes.registerType("dim-xml-cmd-client", DimXmlCmdClient);
};
