"use strict";

const
  { packets } = require('dim'),
  { invoke, get } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimXmlConf').ConfNode} ConfNode
 * @typedef {import('dim').DicValue<any>} DicValue
 * @typedef {import('dim').DnsClient} DnsClient
 *
 * @typedef {nodeRedDimXml.DimXmlClientDef & nodeRedDimXml.DicValueOpts} DicXmlValueDef
 * @typedef {nodeRedDimXml.DimXmlClientNode & nodeRedDimXml.DicValueOpts} DicXmlValueNode
 * @typedef {{timeout: number, stamped: boolean, type?: packets.DicReq.Type}} Options
 */

/**
 *
 * @brief constructor function of DIM-XML Value Client Node
 * @param {Red} RED
 * @param {DicXmlValueDef} c
 * @param {new (service: string, options: Options, dns: DnsClient) => DicValue} Client
 * @param {string} nodeId - node identifier for i18n purpose
 * @this {DicXmlValueNode & {client: DicValue}}
 */
/* eslint-disable-next-line max-statements */
module.exports = function(RED, c, Client, nodeId) {
  RED.nodes.createNode(this, c);
  this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
  this.serviceName = c.serviceName;
  this.monitored = c.monitored;
  this.scheduled = c.scheduled;
  this.timeout = c.timeout; /* ms */
  this.stamped = c.stamped;

  const node = this;

  if (node.cnf) {
    node._removeConnStatus = node.cnf.addConnStatus(node);
    node.dnsClient = node.cnf.getDnsClient();
    if (node.dnsClient) {
      node.cnf.ref();

      /** @type {Options} */
      const options = {
        timeout: 0,
        stamped: node.stamped,
        type: undefined
      };

      if (node.monitored) {
        options.type = packets.DicReq.Type.MONITORED;
        if (node.scheduled) {
          options.timeout = node.timeout;
        }
      }
      else if (node.scheduled) {
        options.type = packets.DicReq.Type.TIMED_ONLY;
        options.timeout = node.timeout;
      }

      /** @type {function(any): void} */
      const sendMsg = (val) => {
        /** @type {{ _msgid: string, payload?: any, timestamp?: number? }} */
        const msg = { _msgid: RED.util.generateId(), payload: val };

        if (node.client && node.stamped) {
          msg.timestamp = get(node.client, 'timestamp');
        }
        node.send(msg);
      };

      if (options.type) {
        node.client = new Client(node.serviceName, options, node.dnsClient);
        node.client.on('value', sendMsg);

        /* check on the first value */
        (async function() {
          await node.client.promise();
        }())
        .catch(() => {
          invoke(node.client, 'removeListener', 'value', sendMsg);
          node.error(RED._(`${nodeId}.error.service-released`,
            { error: node.serviceName }));
        });
      }
      else {
        node.dnsClient.query(node.serviceName)
        .catch((err) => { node.error(err); });
      }

      node.on('input', function(msg, send, done) {
        // @ts-ignore: 'get' exists on DicValue
        Client.get(node.serviceName, node.dnsClient)
        .then((/** @type {any} */val) => { sendMsg(val); done(); })
        .catch(done);
      });

      node.on('close', (/** @type {() => void} */ done) => {
        if (node.client) {
          node.client.removeListener('value', sendMsg);
          node.client.release();
        }
        if (node._removeConnStatus) {
          node._removeConnStatus();
          delete node._removeConnStatus;
        }
        invoke(node.cnf, 'unref');
        node.dnsClient = null;
        done();
      });

      RED.httpAdmin.post("/" + this.type + "/:id", function(req, res) {
        var node = RED.nodes.getNode(req.params.id);
        if (node !== null) {
          try {
            /* trigger an 'input' event */
            node.receive();
            res.sendStatus(200);
          }
          catch (err) {
            res.sendStatus(500);
            node.error(RED._(`${nodeId}.error.fetch-failed-with-err`,
              { error: err.toString() }));
          }
        }
        else {
          res.sendStatus(404);
        }
      });
    }
  }
  else {
    node.error(RED._("node-red-dim-xml/dimXmlConf:dimXmlConf.error.config-missing"));
  }
};
