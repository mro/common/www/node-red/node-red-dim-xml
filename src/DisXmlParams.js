"use strict";

const
  { DicXmlParams } = require('dim-xml'),
  { invoke, isNil } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimXmlConf').ConfNode} ConfNode
 * @typedef {import('dim-xml').XmlDataSet} XmlDataSet
 * @typedef {import('dim-xml').DisXmlDataSet.UpdatePart} UpdatePart
 *
 * @typedef {nodeRedDimXml.DimXmlServerDef & ExtraProps} DisXmlParamsDef
 * @typedef {nodeRedDimXml.DimXmlServerNode & ExtraProps} DisXmlParamsNode
 * @typedef {{ params: XmlDataSet }} ExtraProps
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {
  /**
   * @brief constructor function of DIM-XML Params Server Node
   * @param {DisXmlParamsDef} c
   * @this {DisXmlParamsNode}
   */
  function DisXmlParams(c) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    this.params = c.params;
    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);
      node.disXmlNode = node.cnf.getDisXmlNode();
      if (node.disXmlNode) {
        node.cnf.ref();
        const srv = node.disXmlNode.addXmlParams(node.serviceName);
        const up = (/** @type {any} */val) => {
          const params = DicXmlParams.parse(val || '');
          if (isNil(params)) {
            node.error(RED._("disXmlParams.error.updated-value"));
          }
          else {
            node.send({ _msgid: RED.util.generateId(), payload: params });
          }
        };

        if (!srv) {
          invoke(node, '_removeConnStatus');
          if (invoke(node.disXmlNode, 'isService', node.serviceName + '/Aqn') ||
            invoke(node.disXmlNode, 'isService', node.serviceName + '/Cmd') ||
            invoke(node.disXmlNode, 'isService', node.serviceName + '/Ack')) {
            node.error(RED._("disXmlParams.error.dup-service",
              { service: node.serviceName }));
          }
          else {
            node.error(RED._("disXmlParams.error.service-reg-failed",
              { service: node.serviceName }));
          }
          node.disXmlNode = null;
        }
        else {
          srv.on('update', up);

          /* initialize the Params service */
          node.params.forEach((data) => srv.add(data));

          node.on('input', function(msg, send, done) {
            if (msg.hasOwnProperty("payload")) {
              srv.update(/** @type {UpdatePart}*/(msg.payload));
            }
            done();
          });
        }

        node.on('close', function(/** @type {() => void} */ done) {
          invoke(srv, 'removeListener', 'update', up);
          invoke(node.disXmlNode, 'removeXmlParams', node.serviceName);
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.disXmlNode = null;
          done();
        });
      }
    }
    else {
      node.error(RED._("node-red-dim-xml/dimXmlConf:dimXmlConf.error.config-missing"));
    }
  }
  RED.nodes.registerType("dim-xml-params-server", DisXmlParams);
};
