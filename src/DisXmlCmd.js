"use strict";

const
  { xml } = require('dim-xml'),
  TIMEOUT = require('dim-xml').DicXmlCmd.TIMEOUT,
  { makeDeferred, timeout } = require('prom'),
  { invoke, omit, get, toString, defaultTo } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimXmlConf').ConfNode} ConfNode
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM-XML CMD Server Node
   * @param {nodeRedDimXml.DimXmlServerDef} c
   * @param {boolean} ackDeferred
   * @this {nodeRedDimXml.DimXmlServerNode}
   */
  function DimXmlCmdServer(c, ackDeferred = false) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);
      node.disXmlNode = node.cnf.getDisXmlNode();

      if (node.disXmlNode) {
        node.cnf.ref();
        const cmdSrv = node.disXmlNode.addXmlCmd(node.serviceName,
          async (req) => {
            const d = ackDeferred ? makeDeferred() : null;
            const cmd = xml.toJs(req.xml);
            /** @type {Promise<any>=} */
            let ack;

            /** @type {{
             *  _msgid: string, key: string,
             *  payload: unknown, res?: prom.Deferred
             * }}
             */
            var msg = {
              _msgid: RED.util.generateId(),
              key: get(cmd['$'], 'key', ""),
              payload: omit(cmd, [ '$' ]) /* parameters */
            };
            if (d) {
              msg.res = d;
              ack = timeout(d.promise, defaultTo(req.timeout, TIMEOUT));
            }
            node.send(msg);

            return ack;
          }
        );

        if (!cmdSrv) {
          invoke(node, '_removeConnStatus');
          if (invoke(node.disXmlNode, 'isService', node.serviceName + '/Cmd') ||
            invoke(node.disXmlNode, 'isService', node.serviceName + '/Ack')) {
            node.error(RED._("disXmlCmd.error.dup-service",
              { service: node.serviceName }));
          }
          else {
            node.error(RED._("disXmlCmd.error.service-reg-failed",
              { service: node.serviceName }));
          }
          node.disXmlNode = null;
        }

        node.on('close', function(/** @type {() => void} */ done) {
          invoke(node.disXmlNode, 'removeXmlCmd', node.serviceName);
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.disXmlNode = null;
          done();
        });
      }
    }
    else {
      node.error(RED._("node-red-dim-xml/dimXmlConf:dimXmlConf.error.config-missing"));
    }
  }
  RED.nodes.registerType("dim-xml-cmd-server", DimXmlCmdServer);


  /**
   * @brief constructor function of DIM-XML CMD In Node
   * @param {nodeRedDimXml.DimXmlServerDef} c
   * @this {nodeRedDimXml.DimXmlServerNode}
   */
  function DimXmlCmdIn(c) {
    DimXmlCmdServer.call(this, c, true);
  }
  RED.nodes.registerType("dim-xml-cmd in", DimXmlCmdIn);


  /**
   * @brief constructor function of DIM-XML CMD Response Node
   * @param {nodeRedDimXml.DimXmlServerDef} c
   * @this {nodeRedDimXml.DimXmlServerNode}
   */
  function DimXmlCmdRes(c) {
    RED.nodes.createNode(this, c);
    const node = this;

    node.on('input', function(msg, send, done) {
      if (msg.hasOwnProperty("payload")) {
        // @ts-ignore: 'res' checked in the first condition
        if ((get(msg, 'res.promise') instanceof Promise) && msg.res.isPending) {
          try {
            const error = get(msg, 'error');
            msg.payload = toString(msg.payload);

            if (Number.isInteger(error)) {
              const errMsg = msg.payload;
              // @ts-ignore: 'res' checked above
              msg.res.reject({ message: errMsg, error: error });
              node.debug(RED._("disXmlCmd.error.cmd-failed") + ": " + errMsg);
            }
            else {
              // @ts-ignore: 'res' checked above
              msg.res.resolve(msg.payload);
              node.debug(RED._("disXmlCmd.success.cmd-served"));
            }
            done();
          }
          catch (err) {
            done(err);
          }
        }
        else {
          done(new Error(RED._("disXmlCmd.error.cmd-failed")));
        }
      }
      else {
        done();
      }
    });
  }
  RED.nodes.registerType("dim-xml-cmd-response", DimXmlCmdRes);
};
