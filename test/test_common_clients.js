const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { waitEvent } = require('dim/test/utils'),
  { invoke } = require('lodash'),

  { DnsServer, DnsClient } = require('dim'),
  { DicXmlCmd, DicXmlRpc, DicXmlState, DicXmlDataSet, DicXmlParams,
    DimXmlConf } = require('../src');

describe('DIM-XML Client Nodes', function() {
  var env = {};
  const nodes = [
    { toTest: DicXmlCmd, name: "DIM-XML Cmd Client",
      type: "dim-xml-cmd-client", serviceName: "cmd/test", timeout: "1000",
      confNode: { host: "localhost", port: "2505", retry: "1000" }
    },
    { toTest: DicXmlRpc, name: "DIM-XML Rpc Client",
      type: "dim-xml-rpc-client", serviceName: "rpc/test", timeout: "1000",
      confNode: { host: "localhost", port: "2505", retry: "1000" }
    },
    { toTest: DicXmlState, name: "DIM-XML State Client",
      type: "dim-xml-state-client", serviceName: "state/test", timeout: "1000",
      flags: { monitored: true, scheduled: false, stamped: false },
      confNode: { host: "localhost", port: "2505", retry: "1000" }
    },
    {
      toTest: DicXmlDataSet, name: "DIM-XML DataSet Client",
      type: "dim-xml-dataset-client", serviceName: "dataset/test",
      timeout: "1000",
      flags: { monitored: true, scheduled: false, stamped: false },
      confNode: { host: "localhost", port: "2505", retry: "1000" }
    },
    {
      toTest: DicXmlParams, name: "DIM-XML Params Client",
      type: "dim-xml-params-client", serviceName: "params/test",
      timeout: "1000",
      flags: { monitored: true, scheduled: false, stamped: false },
      confNode: { host: "localhost", port: "2505", retry: "1000" }
    }
  ];

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);

    await env.dns.listen();
    const dnsClient = new DnsClient('localhost', 2505);
    dnsClient.unref();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dns, 'close');
    env = {};
  });

  nodes.forEach((n) => {
    it(`[ ${n.name} ] is loaded correctly`, function(done) {
      const node = { id: "node", type: `${n.type}`, name: `${n.name}`,
        cnf: "conf", serviceName: `${n.serviceName}`, timeout: `${n.timeout}` };
      if (n.flags) { Object.assign(node, n.flags); }

      const flow = [
        { id: "conf", type: "dim-xml-conf", host: `${n.confNode.host}`,
          port: `${n.confNode.port}`, retry: `${n.confNode.retry}` }
      ];
      flow.push(node);

      helper.load([ n.toTest, DimXmlConf ], flow, async function() {
        const node = helper.getNode("node");
        try {
          expect(node).to.have.property('name', `${n.name}`);
          expect(node.cnf).to.have.property('_host', `${n.confNode.host}`);
          expect(node.cnf).to.have.property('_port', `${n.confNode.port}`);
          expect(node.cnf).to.have.property('_retry', `${n.confNode.retry}`);
          expect(node).to.have.property('serviceName', `${n.serviceName}`);
          expect(node).to.have.property('timeout', `${n.timeout}`);
          if (n.flags) {
            expect(node).to.have.property('monitored', n.flags.monitored);
            expect(node).to.have.property('scheduled', n.flags.scheduled);
            expect(node).to.have.property('stamped', n.flags.stamped);
          }

          await waitEvent(node.cnf.getDnsClient(), 'connect');
          expect(node.status.lastCall.args).to.be.deep.equal(
            [ { fill: "green", shape: "dot",
              text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" } ]);

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it(`[ ${n.name} ] raises an error if the Dns Configuration is not set`,
      function(done) {
        const flow = [
          { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
            serviceName: `${n.serviceName}`, timeout: `${n.timeout}` }
        ];

        helper.load(n.toTest, flow, function() {
          const node = helper.getNode("node");
          try {
            expect(node.error.lastCall.args).to.be.deep
            .equal([ 'node-red-dim-xml/dimXmlConf:dimXmlConf.error.config-missing' ]);

            done();
          }
          catch (err) {
            done(err);
          }
        });
      });

    it(`[ ${n.name} ] raises an error if the Dns Configuration is wrong`,
      function(done) {
        const flow = [
          { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
            serviceName: `${n.serviceName}`, timeout: `${n.timeout}` },
          { id: "conf", type: "dim-xml-conf", host: "1234",
            port: `${n.confNode.port}`, retry: `${n.confNode.retry}` }
        ];

        helper.load([ n.toTest, DimXmlConf ], flow, function() {
          const node = helper.getNode("node");

          node.once('call:error', function(call) {
            try {
              expect(call.args.toString())
              .to.be.deep.equal('Error: failed to connect');

              done();
            }
            catch (err) {
              done(err);
            }
          });
        });
      });
  });

});
