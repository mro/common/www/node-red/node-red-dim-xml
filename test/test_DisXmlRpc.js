const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { waitEvent } = require('dim/test/utils'),
  { invoke } = require('lodash'),

  { DnsServer } = require('dim'),
  { DicXmlRpc, xml } = require('dim-xml'),
  { DisXmlRpc, DimXmlConf } = require('../src');

describe('DisXmlRpc Nodes', function() {
  var env = {};

  const rpcParams = [
    { type: 'string', sent: 'TEST', received: { $textContent: 'TEST' } },
    { type: 'number', sent: 1234, received: { $textContent: '1234' } },
    { type: 'boolean', sent: false, received: { $textContent: 'false' } },
    {
      type: 'JS object',
      sent: { num: 1.23, str: 'test', arr: [ 4, 5, 6 ], bool: true,
        obj: { p: 'test' }
      },
      received: { num: '1.23', str: 'test', arr: [ '4', '5', '6' ],
        bool: 'true', obj: { p: 'test' }
      }
    },
    {
      type: 'JS object with text content',
      sent: { num: 1.23, $textContent: "request text content" },
      received: { num: '1.23', $textContent: "request text content" }
    }
  ];

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    await env.dns.listen();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dns, 'close');
    env = {};
  });

  rpcParams.forEach((p) => {
    it(`[ DIM-XML Rpc IN ] can receive a RPC request (param: ${p.type})`,
      function(done) {
        const flow = [
          { id: "disXmlRpc", type: "dim-xml-rpc in", name: "DIM-XML Rpc IN",
            cnf: "conf", serviceName: "rpc/test", wires: [ [ "help" ] ] },
          { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
            retry: "1000" },
          { id: "help", type: "helper" }
        ];

        helper.load([ DisXmlRpc, DimXmlConf ], flow, async function() {
          const disXmlRpcNode = helper.getNode("disXmlRpc");
          const helperNode = helper.getNode("help");

          helperNode.once('input', function(msg) {
            try {
              expect(disXmlRpcNode.send.callCount).to.be.equal(1);
              expect(msg.payload).to.be.deep.equal(p.received);
            }
            catch (err) {
              done(err);
            }
          });

          try {
            await waitEvent(disXmlRpcNode.cnf.getDnsClient(), 'connect');
            expect(disXmlRpcNode.status.lastCall.args).to.be.deep.equal(
              [ { fill: "green", shape: "dot",
                text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" } ]);

            DicXmlRpc.invoke('rpc/test', p.sent, env.dns.url())
            .then(
              () => { expect.fail('should fail'); },
              (err) => {
                expect(err.toString()).to.be.equal('Error: request time-out');
                done();
              }
            ).catch(done);
          }
          catch (err) {
            done(err);
          }
        });
      });
  });

  it(`[ DIM-XML Rpc IN ] removes the service when distroyed`, function(done) {
    const flow = [
      { id: "disXmlRpc", type: "dim-xml-rpc in", name: "DIM-XML Rpc IN",
        cnf: "conf", serviceName: "rpc/test" },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DisXmlRpc, DimXmlConf ], flow, async function() {
      const disXmlRpcNode = helper.getNode("disXmlRpc");
      env.dis = disXmlRpcNode.disXmlNode;
      try {
        await waitEvent(disXmlRpcNode.cnf.getDnsClient(), 'connect');

        expect(env.dis.isService('rpc/test/RpcIn')).to.be.true();
      }
      catch (err) {
        done(err);
      }

      helper.clearFlows() /* destroy the node */
      .then(() => {
        expect(env.dis.isService('rpc/test/RpcIn')).to.be.false();
        done();
      })
      .catch(done);
    });
  });

  it('[ DIM-XML Rpc Response ] can serve a RPC request', function(done) {
    const flow = [
      { id: "dimXmlRpcIn", type: "dim-xml-rpc in", name: "DIM-XML Rpc IN",
        cnf: "conf", serviceName: "rpc/test",
        wires: [ [ "dimXmlRpcRes" ] ] },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "dimXmlRpcRes", type: "dim-xml-rpc-response" }
    ];

    helper.load([ DisXmlRpc, DimXmlConf ], flow, async function() {
      const dimXmlRpcInNode = helper.getNode('dimXmlRpcIn');
      const dimXmlRpcResNode = helper.getNode("dimXmlRpcRes");

      dimXmlRpcResNode.once('call:debug', function(call) {
        try {
          expect(call.args).to.be.deep.equal(
            [ 'disXmlRpc.success.request-served' ]);
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(dimXmlRpcInNode.cnf.getDnsClient(), 'connect');
        const ret = await DicXmlRpc.invoke('rpc/test',
          { num: 123, $textContent: "TEST" }, env.dns.url());

        expect(ret).to.deep.contain({ error: 0, status: 2 });
        const data = xml.toJs(ret.xml);
        expect(data).to.haveOwnProperty('num').that.is.equal('123');
        expect(data).to.haveOwnProperty('$textContent').that.is.equal('TEST');
        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it(`[ DIM-XML Rpc Response ] raises an error if the input message doesn't` +
    ` come from a 'DIM-XML Rpc IN' node`, function(done) {
    const flow = [ { id: "dimXmlRpcRes", type: "dim-xml-rpc-response" } ];

    helper.load(DisXmlRpc, flow, function() {
      const dimXmlRpcResNode = helper.getNode("dimXmlRpcRes");

      dimXmlRpcResNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: disXmlRpc.error.request-failed');

          done();
        }
        catch (err) {
          done(err);
        }
      });

      const msg = { payload: '1234' };
      dimXmlRpcResNode.receive(msg);
    });
  });
});
