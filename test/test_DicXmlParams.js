const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { waitEvent } = require('dim/test/utils'),
  { invoke, differenceBy, sortBy } = require('lodash'),

  { DnsServer, DnsClient } = require('dim'),
  { DisXmlNode, XmlData, XmlCmd } = require('dim-xml'),
  { DicXmlParams, DimXmlConf } = require('../src');

describe('DicXmlParams Node', function() {
  var env = {};

  const params = [
    { index: 0, name: 'sample1', unit: 'mV', type: XmlData.Type.FLOAT,
      value: 1.23 },
    { index: 1, name: 'sample2', unit: 'kS', type: XmlData.Type.UINT32,
      value: 8000 },
    { index: 2, name: 'delay', unit: 'ms', type: XmlData.Type.INT64,
      value: 1500 },
    { index: 3, name: 'active', type: XmlData.Type.BOOL,
      value: true },
    { index: 4, name: 'moduleType', type: XmlData.Type.STRING,
      value: 'ACQC' },
    { index: 5, name: 'level', type: XmlData.Type.ENUM,
      value: 1, valueName: 'medium', enum: [
        { value: 0, name: 'low' },
        { value: 1, name: 'medium' },
        { value: 2, name: 'high' }
      ]
    },
    { index: 6, name: 'channel', value: [
      { index: 0, name: 'detectorId', type: XmlData.Type.UINT32,
        value: 1 },
      { index: 1, name: 'threshold', unit: 'mV', type: XmlData.Type.INT32,
        value: 130 }
    ] }
  ];

  const tests = [
    { name: "float data",
      inputValue: { index: 0, value: 5.67, type: XmlData.Type.FLOAT },
      expectedValue: [
        { index: 0, name: 'sample1', unit: 'mV', type: XmlData.Type.FLOAT,
          value: 5.67 },
        /* copy the remaining elements */
        ...differenceBy(params, [ { index: 0 } ], 'index')
      ]
    },
    { name: "unsigned integer data",
      inputValue: { index: 1, value: 5000, type: XmlData.Type.UINT32 },
      expectedValue: sortBy([
        { index: 1, name: 'sample2', unit: 'kS', type: XmlData.Type.UINT32,
          value: 5000 },
        ...differenceBy(params, [ { index: 1 } ], 'index')
      ], 'index')
    },
    { name: "integer data",
      inputValue: { index: 2, value: 100, type: XmlData.Type.INT64 },
      expectedValue: sortBy([
        { index: 2, name: 'delay', unit: 'ms', type: XmlData.Type.INT64,
          value: 100 },
        ...differenceBy(params, [ { index: 2 } ], 'index')
      ], 'index')
    },
    { name: "boolean data",
      inputValue: { index: 3, value: false, type: XmlData.Type.BOOL },
      expectedValue: sortBy([
        { index: 3, name: 'active', type: XmlData.Type.BOOL, value: false },
        ...differenceBy(params, [ { index: 3 } ], 'index')
      ], 'index')
    },
    { name: "string data",
      inputValue: { index: 4, value: "SPDV", type: XmlData.Type.STRING },
      expectedValue: sortBy([
        { index: 4, name: 'moduleType', type: XmlData.Type.STRING,
          value: "SPDV" },
        ...differenceBy(params, [ { index: 4 } ], 'index')
      ], 'index')
    },
    { name: "enum data",
      inputValue: { index: 5, value: 2, type: XmlData.Type.ENUM },
      expectedValue: sortBy([
        { index: 5, name: 'level', type: XmlData.Type.ENUM,
          value: 2, valueName: "high", enum: [
            { value: 0, name: 'low' },
            { value: 1, name: 'medium' },
            { value: 2, name: 'high' }
          ] },
        ...differenceBy(params, [ { index: 5 } ], 'index')
      ], 'index')
    },
    { name: "nested data",
      inputValue: { index: 6, value: [
        { index: 1, value: 250, type: XmlData.Type.INT32 }
      ] },
      expectedValue: sortBy([
        { index: 6, name: 'channel', value: [
          { index: 0, name: 'detectorId', type: XmlData.Type.UINT32, value: 1 },
          { index: 1, name: 'threshold', unit: 'mV', type: XmlData.Type.INT32,
            value: 250 }
        ] },
        ...differenceBy(params, [ { index: 6 } ], 'index')
      ], 'index')
    }
  ];

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    env.dis = new DisXmlNode();

    await env.dns.listen();
    const dnsClient = new DnsClient('localhost', 2505);
    await dnsClient.register(env.dis);
    dnsClient.unref();

    const srv = env.dis.addXmlParams('params/test');
    params.forEach((p) => srv.add(p));
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dns, 'close');
    env = {};
  });

  describe('can update parameters', function() {
    tests.forEach((t) => {
      it(`${t.name}`, function(done) {
        const flow = [
          { id: "dicXmlParams", type: "dim-xml-params-client",
            name: "DIM-XML Params Client", cnf: "conf",
            serviceName: "params/test", timeout: "1000", monitored: true,
            scheduled: false, stamped: false, wires: [ [ "help" ] ] },
          { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
            retry: "1000" },
          { id: "help", type: "helper" }
        ];

        helper.load([ DicXmlParams, DimXmlConf ], flow, async function() {
          const dicXmlParamsNode = helper.getNode("dicXmlParams");
          const helperNode = helper.getNode("help");

          try {
            await waitEvent(helperNode, 'input'); // wait for first value
          }
          catch (err) {
            done(err);
          }

          helperNode.once('input', function(msg) {
            try {
              expect(dicXmlParamsNode.send.callCount).to.be.equal(2);
              expect(msg.payload).to.deep.equal(t.expectedValue);
              done();
            }
            catch (err) {
              done(err);
            }
          });

          dicXmlParamsNode.receive({ payload: t.inputValue });
        });
      });
    });
  });

  it('can handle errors', function(done) {
    const flow = [
      { id: "dicXmlParams", type: "dim-xml-params-client",
        name: "DIM-XML Params Client", cnf: "conf",
        serviceName: "params/test", timeout: "1000", monitored: true,
        scheduled: false, stamped: false, wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicXmlParams, DimXmlConf ], flow, async function() {
      const dicXmlParamsNode = helper.getNode("dicXmlParams");
      const helperNode = helper.getNode("help");

      try {
        await waitEvent(helperNode, 'input'); // wait for first value
      }
      catch (err) {
        done(err);
      }

      helperNode.once('input', function(msg) {
        try {
          expect(dicXmlParamsNode.send.callCount).to.be.equal(2);
          expect(msg).to.deep.contains({
            payload: [],
            status: XmlCmd.Status.ERROR,
            error: -1
          });
          done();
        }
        catch (err) {
          done(err);
        }
      });

      // try to update data without 'type' attribute
      dicXmlParamsNode.receive({ payload: { index: 0, value: 5.67 } });
    });
  });
});

