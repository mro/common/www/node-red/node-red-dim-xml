const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { waitForValue } = require('./utils'),

  { invoke, differenceBy, sortBy } = require('lodash'),

  { DnsServer } = require('dim'),
  { DicXmlDataSet, XmlData } = require('dim-xml'),
  { DisXmlDataSet, DimXmlConf } = require('../src');

describe('DisXmlDataSet Node', function() {
  var env = {};

  const dataset = [
    { index: 0, name: 'sample1', unit: 'mV', type: XmlData.Type.FLOAT,
      value: 1.23 },
    { index: 1, name: 'sample2', unit: 'kS', type: XmlData.Type.UINT32,
      value: 8000 },
    { index: 2, name: 'delay', unit: 'ms', type: XmlData.Type.INT64,
      value: 1500 },
    { index: 3, name: 'active', unit: '', type: XmlData.Type.BOOL,
      value: true },
    { index: 4, name: 'moduleType', unit: '', type: XmlData.Type.STRING,
      value: 'ACQC' },
    { index: 5, name: 'level', unit: '', type: XmlData.Type.ENUM,
      value: 1, valueName: 'medium', enum: [
        { value: 0, name: 'low' },
        { value: 1, name: 'medium' },
        { value: 2, name: 'high' }
      ]
    },
    { index: 6, name: 'channel', unit: '', type: XmlData.Type.NESTED, value: [
      { index: 0, name: 'detectorId', unit: '', type: XmlData.Type.UINT32,
        value: 1 },
      { index: 1, name: 'threshold', unit: 'mV', type: XmlData.Type.INT32,
        value: 130 }
    ] }
  ];

  const firstValue = [
    { index: 0, name: 'sample1', unit: 'mV', type: XmlData.Type.FLOAT,
      value: 1.23 },
    { index: 1, name: 'sample2', unit: 'kS', type: XmlData.Type.UINT32,
      value: 8000 },
    { index: 2, name: 'delay', unit: 'ms', type: XmlData.Type.INT64,
      value: 1500 },
    { index: 3, name: 'active', type: XmlData.Type.BOOL, value: true },
    { index: 4, name: 'moduleType', type: XmlData.Type.STRING,
      value: 'ACQC' },
    { index: 5, name: 'level', type: XmlData.Type.ENUM, value: 1,
      valueName: 'medium', enum: [
        { value: 0, name: 'low' },
        { value: 1, name: 'medium' },
        { value: 2, name: 'high' }
      ]
    },
    { index: 6, name: 'channel', value: [
      { index: 0, name: 'detectorId', type: XmlData.Type.UINT32, value: 1 },
      { index: 1, name: 'threshold', unit: 'mV', type: XmlData.Type.INT32,
        value: 130 }
    ] }
  ];

  const tests = [
    { name: "float data", inputValue: { index: 0, value: 5.67 },
      expectedValue: [
        { index: 0, name: 'sample1', unit: 'mV', type: XmlData.Type.FLOAT,
          value: 5.67 },
        /* copy the remaining elements */
        ...differenceBy(firstValue, [ { index: 0 } ], 'index')
      ]
    },
    { name: "unsigned integer data", inputValue: { index: 1, value: 5000 },
      expectedValue: sortBy([
        { index: 1, name: 'sample2', unit: 'kS', type: XmlData.Type.UINT32,
          value: 5000 },
        ...differenceBy(firstValue, [ { index: 1 } ], 'index')
      ], 'index')
    },
    { name: "integer data", inputValue: { index: 2, value: 100 },
      expectedValue: sortBy([
        { index: 2, name: 'delay', unit: 'ms', type: XmlData.Type.INT64,
          value: 100 },
        ...differenceBy(firstValue, [ { index: 2 } ], 'index')
      ], 'index')
    },
    { name: "boolean data", inputValue: { index: 3, value: false },
      expectedValue: sortBy([
        { index: 3, name: 'active', type: XmlData.Type.BOOL, value: false },
        ...differenceBy(firstValue, [ { index: 3 } ], 'index')
      ], 'index')
    },
    { name: "string data", inputValue: { index: 4, value: "SPDV" },
      expectedValue: sortBy([
        { index: 4, name: 'moduleType', type: XmlData.Type.STRING,
          value: "SPDV" },
        ...differenceBy(firstValue, [ { index: 4 } ], 'index')
      ], 'index')
    },
    { name: "enum data", inputValue: { index: 5, value: 2 },
      expectedValue: sortBy([
        { index: 5, name: 'level', type: XmlData.Type.ENUM,
          value: 2, valueName: "high", enum: [
            { value: 0, name: 'low' },
            { value: 1, name: 'medium' },
            { value: 2, name: 'high' }
          ] },
        ...differenceBy(firstValue, [ { index: 5 } ], 'index')
      ], 'index')
    },
    { name: "nested data",
      inputValue: { index: 6, value: [ { index: 1, value: 250 } ] },
      expectedValue: sortBy([
        { index: 6, name: 'channel', value: [
          { index: 0, name: 'detectorId', type: XmlData.Type.UINT32, value: 1 },
          { index: 1, name: 'threshold', unit: 'mV', type: XmlData.Type.INT32,
            value: 250 }
        ] },
        ...differenceBy(firstValue, [ { index: 6 } ], 'index')
      ], 'index')
    }
  ];

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    await env.dns.listen();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dns, 'close');
    invoke(env.dis, 'close');
    invoke(env.dicXmlDataSet, 'release');
    env = {};
  });

  describe('sends updates when dataset changes', function() {
    tests.forEach((t) => {
      it(`${t.name}`, function(done) {
        const service = 'dataset/test';
        const flow = [
          { id: "disXmlDataSet", type: "dim-xml-dataset-server",
            name: "DIM-XML DataSet Server", cnf: "conf", serviceName: service,
            dataset: dataset, wires: [ [ "help" ] ] },
          { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
            retry: "1000" },
          { id: "help", type: "helper" }
        ];

        env.dicXmlDataSet = new DicXmlDataSet(service, null, env.dns.url());
        helper.load([ DisXmlDataSet, DimXmlConf ], flow, function() {
          const disXmlDataSetNode = helper.getNode("disXmlDataSet");
          const helperNode = helper.getNode("help");
          /* Check initial value */
          env.dicXmlDataSet.then(
            (val) => expect(val).to.be.deep.equal(firstValue)
          )
          .then(() => {
            const prom = waitForValue(env.dicXmlDataSet, 'value', (val) => val,
              t.expectedValue);

            helperNode.once('input', async function(msg) {
              try {
                expect(msg.payload).to.be.an('array')
                .and.to.be.deep.equal(t.expectedValue);
                await prom;

                done();
              }
              catch (err) {
                done(err);
              }
            });

            /* update value */
            disXmlDataSetNode.receive({ payload: t.inputValue });
          }).catch(done);
        });
      });
    });
  });

  it('removes the service when distroyed', function(done) {
    const flow = [
      { id: "disXmlDataSet", type: "dim-xml-dataset-server",
        name: "DIM-XML DataSet Server", cnf: "conf",
        serviceName: "dataset/test", dataset: dataset },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DisXmlDataSet, DimXmlConf ], flow, function() {
      const disXmlDataSetNode = helper.getNode("disXmlDataSet");
      env.dis = disXmlDataSetNode.disXmlNode;

      try {
        expect(env.dis.isService('dataset/test')).to.be.true();
      }
      catch (err) {
        done(err);
      }

      helper.clearFlows() /* destroy the node */
      .then(() => {
        expect(env.dis.isService('dataset/test')).to.be.false();
        done();
      })
      .catch(done);
    });
  });
});
