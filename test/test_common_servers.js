const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { waitEvent } = require('dim/test/utils'),
  { invoke } = require('lodash'),

  { DnsServer, DnsClient, NodeInfo } = require('dim'),
  { DisXmlNode } = require('dim-xml'),
  { DisXmlCmd, DisXmlRpc, DisXmlState, DisXmlDataSet, DisXmlParams,
    DimXmlConf } = require('../src');

describe('DIM-XML Server Nodes', function() {
  var env = {};

  const nodes = [
    { toTest: DisXmlCmd, name: "DIM-XML Cmd Server",
      type: "dim-xml-cmd-server", serviceName: "cmd/test",
      confNode: { host: "localhost", port: "2505", retry: "1000" },
      addService: DisXmlNode.prototype.addXmlCmd, cb: () => {}
    },
    { toTest: DisXmlCmd, name: "DIM-XML Cmd IN",
      type: "dim-xml-cmd in", serviceName: "cmd/test",
      confNode: { host: "localhost", port: "2505", retry: "1000" },
      addService: DisXmlNode.prototype.addXmlCmd, cb: () => {}
    },
    { toTest: DisXmlCmd, name: "DIM-XML Cmd Response",
      type: "dim-xml-cmd-response",
      addService: DisXmlNode.prototype.addXmlCmd
    },
    { toTest: DisXmlRpc, name: "DIM-XML Rpc IN",
      type: "dim-xml-rpc in", serviceName: "rpc/test",
      confNode: { host: "localhost", port: "2505", retry: "1000" },
      addService: DisXmlNode.prototype.addXmlRpc, cb: () => {}
    },
    { toTest: DisXmlRpc, name: "DIM-XML Rpc Response",
      type: "dim-xml-rpc-response",
      addService: DisXmlNode.prototype.addXmlRpc
    },
    { toTest: DisXmlState, name: "DIM-XML State Server",
      type: "dim-xml-state-server", serviceName: "state/test",
      value: {
        initState: "0",
        states: [
          { value: "0", name: "idle" },
          { value: "1", name: "processing" }
        ]
      },
      confNode: { host: "localhost", port: "2505", retry: "1000" },
      addService: DisXmlNode.prototype.addXmlState
    },
    { toTest: DisXmlDataSet, name: "DIM-XML DataSet Server",
      type: "dim-xml-dataset-server", serviceName: "dataset/test",
      value: {
        dataset: [
          { index: 0, name: 'sampleRate', unit: 'MS/s', type: 8, value: 1000 },
          { index: 1, name: 'sampleSize', unit: 'kS', type: 11, value: 7000 }
        ]
      },
      confNode: { host: "localhost", port: "2505", retry: "1000" },
      addService: DisXmlNode.prototype.addXmlDataSet
    },
    { toTest: DisXmlParams, name: "DIM-XML Params Server",
      type: "dim-xml-params-server", serviceName: "params/test",
      value: {
        params: [
          { index: 0, name: 'sampleRate', unit: 'MS/s', type: 8, value: 1000 },
          { index: 1, name: 'sampleSize', unit: 'kS', type: 11, value: 7000 }
        ]
      },
      confNode: { host: "localhost", port: "2505", retry: "1000" },
      addService: DisXmlNode.prototype.addXmlParams
    }
  ];

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    await env.dns.listen();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dns, 'close');
    delete env.dis;
    delete env.dns;
  });

  nodes.forEach((n) => {
    it(`[ ${n.name} ] is loaded correctly`, function(done) {
      const flow = [];
      const node = { id: "node", type: `${n.type}`, name: `${n.name}`,
        cnf: "conf", serviceName: `${n.serviceName}` };

      if (n.value) { Object.assign(node, n.value); }
      flow.push(node);

      if (n.confNode) {
        flow.push({ id: "conf", type: "dim-xml-conf",
          host: `${n.confNode.host}`,
          port: `${n.confNode.port}`,
          retry: `${n.confNode.retry}`
        });
      }

      helper.load([ n.toTest, DimXmlConf ], flow, async function() {
        const node = helper.getNode("node");
        try {
          expect(node).to.have.property('name', `${n.name}`);
          if (n.serviceName) {
            expect(node).to.have.property('serviceName', `${n.serviceName}`);
          }
          if (n.confNode) {
            expect(node.cnf).to.have.property('_host', `${n.confNode.host}`);
            expect(node.cnf).to.have.property('_port', `${n.confNode.port}`);
            expect(node.cnf).to.have.property('_retry', `${n.confNode.retry}`);

            await waitEvent(node.cnf.getDnsClient(), 'connect');
            expect(node.status.lastCall.args).to.be.deep.equal(
              [ { fill: "green", shape: "dot",
                text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" } ]);
          }
          if (n.value) {
            for (const p in n.value) {
              if (n.value.hasOwnProperty(p)) {
                expect(node).to.have.property(p).that.is.deep.equal(n.value[p]);
              }
            }
          }
          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    if (n.confNode) {
      it(`[ ${n.name} ] raises an error if the Dns Configuration is not set`,
        function(done) {
          const flow = [];
          const node = { id: "node", type: `${n.type}`, name: `${n.name}`,
            cnf: "conf", serviceName: `${n.serviceName}` };

          if (n.value) { Object.assign(node, n.value); }
          flow.push(node);

          helper.load(n.toTest, flow, function() {
            const node = helper.getNode("node");
            try {
              expect(node.error.lastCall.args).to.be.deep
              .equal([ 'node-red-dim-xml/dimXmlConf:dimXmlConf.error.config-missing' ]);

              done();
            }
            catch (err) {
              done(err);
            }
          });
        });

      it(`[ ${n.name} ] raises an error in case of duplicate taskNames`,
        function(done) {
          const flow = [
            { id: "conf", type: "dim-xml-conf", host: `${n.confNode.host}`,
              port: `${n.confNode.port}`, retry: `${n.confNode.retry}` }
          ];
          const node = { id: "node", type: `${n.type}`, name: `${n.name}`,
            cnf: "conf", serviceName: `${n.serviceName}` };

          if (n.value) { Object.assign(node, n.value); }
          flow.push(node);

          env.dis = new DisXmlNode();
          n.addService.call(env.dis, n.serviceName, n.cb);

          let dnsClient = new DnsClient('localhost', 2505);
          dnsClient.register(env.dis).then(() => {
            dnsClient.unref();
            dnsClient = null;

            helper.load([ n.toTest, DimXmlConf ], flow, function() {
              const node = helper.getNode("node");
              const taskName = node.disXmlNode.info.task;

              node.cnf.on('call:error', function(call) {
                try {
                  expect(call.args).to.be.deep.equal(
                    [ 'dimXmlConf.error.dup-taskName' ]);

                  done();
                }
                catch (err) {
                  done(err);
                }
              });

              try {
                expect(env.dis.info.task).to.be.deep.equal(taskName);
              }
              catch (err) {
                done(err);
              }
            });
          }).catch(done);
        });

      it(`[ ${n.name} ] raises an error in case of duplicate services on` +
        ` different DIM Service Providers`, function(done) {
        const flow = [
          { id: "conf", type: "dim-xml-conf", host: `${n.confNode.host}`,
            port: `${n.confNode.port}`, retry: `${n.confNode.retry}` }
        ];
        const node = { id: "node", type: `${n.type}`, name: `${n.name}`,
          cnf: "conf", serviceName: `${n.serviceName}` };

        if (n.value) { Object.assign(node, n.value); }
        flow.push(node);

        env.dis = new DisXmlNode(NodeInfo.local(0, 0, 'taskName2'));
        n.addService.call(env.dis, n.serviceName, n.cb);

        let dnsClient = new DnsClient('localhost', 2505);
        dnsClient.register(env.dis).then(() => {
          dnsClient.unref();
          dnsClient = null;

          helper.load([ n.toTest, DimXmlConf ], flow, function() {
            const node = helper.getNode("node");

            node.cnf.once('call:error', function(call) {
              try {
                expect(call.args).to.deep
                .equal([ 'dimXmlConf.error.dup-service' ]);
                done();
              }
              catch (err) {
                done(err);
              }
            });
          });
        }).catch(done);
      });

      it(`[ ${n.name} ] raises an error in case of duplicate services on the` +
        ` same DIM Service Provider`, function(done) {
        const flow = [
          /* config node */
          { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
            retry: "1000" }
        ];
        /* first node */
        const n1 = { id: "node1", type: `${n.type}`, name: `${n.name}1`,
          cnf: "conf", serviceName: `${n.serviceName}` };
        /* second node */
        const n2 = { id: "node2", type: `${n.type}`, name: `${n.name}2`,
          cnf: "conf", serviceName: `${n.serviceName}` };

        if (n.value) {
          Object.assign(n1, n.value);
          Object.assign(n2, n.value);
        }
        flow.push(n1, n2);

        helper.load([ n.toTest, DimXmlConf ], flow, function() {
          const node1 = helper.getNode("node1");
          const node2 = helper.getNode("node2");

          var f1 = function(call) {
            node2.removeListener('call:error', f2);
            try {
              expect(call.firstArg.toString())
              .to.contains('.error.dup-service');
              done();
            }
            catch (err) {
              done(err);
            }
          };

          var f2 = function(call) {
            node1.removeListener('call:error', f1);
            try {
              expect(call.firstArg.toString())
              .to.contains('.error.dup-service');
              done();
            }
            catch (err) {
              done(err);
            }
          };

          node1.once('call:error', f1);
          node2.once('call:error', f2);
        });
      });
    }
  });
});
