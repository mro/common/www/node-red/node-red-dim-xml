/* eslint-disable max-lines */
const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { waitEvent } = require('dim/test/utils'),
  { waitForValue } = require('./utils'),

  { invoke } = require('lodash'),

  { DnsServer } = require('dim'),
  { DicXmlState } = require('dim-xml'),
  { DisXmlState, DimXmlConf } = require('../src');

describe('DisXmlState Node', function() {
  var env = {};
  const states = [
    { value: "0", name: "Initialization" },
    { value: "1", name: "Idle" },
    { value: "2", name: "Calibration" },
    { value: "3", name: "Processing" }
  ];

  const firstValue = {
    description: [
      { strValue: "ERROR", value: -2 },
      { strValue: "NOT READY", value: -1 },
      { strValue: "Initialization", value: 0 },
      { strValue: "Idle", value: 1 },
      { strValue: "Calibration", value: 2 },
      { strValue: "Processing", value: 3 }
    ],
    strValue: "Initialization",
    value: 0,
    errors: [],
    warnings: []
  };

  const tests = [
    { name: "the state changes", inputValue: { state: 1 },
      expectedValue: { ...firstValue, strValue: "Idle", value: 1 } },
    { name: "it receives an error",
      inputValue: { error: { code: "500", message: "Server Error" } },
      expectedValue: { ...firstValue, strValue: "ERROR", value: -2,
        errors: [ { code: 500, message: "Server Error" } ] } },
    { name: "it receives an error list",
      inputValue: {
        errors: [
          { code: 500, message: "Server Error" },
          { code: 404, message: "Not Found" }
        ]
      },
      expectedValue: { ...firstValue, strValue: "ERROR", value: -2,
        errors: [
          { code: 500, message: "Server Error" },
          { code: 404, message: "Not Found" }
        ]
      } },
    { name: "it receives a warning",
      inputValue: { warning: { code: "199", message: "Non-specific warning" } },
      expectedValue: { ...firstValue,
        warnings: [ { code: 199, message: "Non-specific warning" } ] } },
    { name: "it receives a warning list",
      inputValue: {
        warnings: [
          { code: 199, message: "Warning 1" },
          { code: 800, message: "Warning 2" }
        ]
      },
      expectedValue: { ...firstValue,
        warnings: [
          { code: 199, message: "Warning 1" },
          { code: 800, message: "Warning 2" }
        ]
      } }
  ];

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    await env.dns.listen();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dns, 'close');
    invoke(env.dis, 'close');
    invoke(env.dicXmlState, 'release');
    env = {};
  });

  describe('sends updates when', function() {
    tests.forEach((t) => {
      it(`${t.name}`, function(done) {
        const service = 'state/test';
        const flow = [
          { id: "disXmlState", type: "dim-xml-state-server",
            name: "DIM-XML State Server", cnf: "conf", serviceName: service,
            initState: "0", states: states, wires: [ [ "help" ] ] },
          { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
            retry: "1000" },
          { id: "help", type: "helper" }
        ];

        env.dicXmlState = new DicXmlState(service, null, env.dns.url());

        helper.load([ DisXmlState, DimXmlConf ], flow, function() {
          const disXmlStateNode = helper.getNode("disXmlState");
          const helperNode = helper.getNode("help");
          /* Check initial value */
          env.dicXmlState.then(
            (val) => expect(val).to.be.deep.equal(firstValue)
          )
          .then(() => {
            const prom = waitForValue(env.dicXmlState, 'value', (val) => val,
              t.expectedValue);

            helperNode.once('input', async function(msg) {
              try {
                expect(msg.payload).to.be.deep.equal(t.expectedValue);
                await prom;

                done();
              }
              catch (err) {
                done(err);
              }
            });

            /* update value */
            disXmlStateNode.receive({ payload: t.inputValue });
          }).catch(done);
        });
      });
    });
  });

  it('can resets warnings', function(done) {
    const service = 'state/test';
    const flow = [
      { id: "disXmlState", type: "dim-xml-state-server",
        name: "DIM-XML State Server", cnf: "conf", serviceName: service,
        initState: "0", states: states },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dicXmlState = new DicXmlState(service, null, env.dns.url());

    helper.load([ DisXmlState, DimXmlConf ], flow, function() {
      const disXmlStateNode = helper.getNode("disXmlState");
      /* Check initial value */
      env.dicXmlState.then(
        (val) => expect(val).to.be.deep.equal(firstValue)
      )
      .then(async function() {
        const prom = waitForValue(env.dicXmlState, 'value', (val) => val,
          { ...firstValue, warnings: [
            { code: 199, message: "Warning 1" },
            { code: 800, message: "Warning 2" }
          ] });

        /* update value */
        disXmlStateNode.receive({
          payload: {
            warnings: [
              { code: 199, message: "Warning 1" },
              { code: 800, message: "Warning 2" }
            ]
          }
        });
        await prom;
      })
      .then(async function() {
        const prom = waitForValue(env.dicXmlState, 'value', (val) => val,
          { ...firstValue, strValue: "Idle", value: 1, warnings: [] });

        /* update value */
        disXmlStateNode.receive({ payload: { state: 1, warnings: [] } });
        await prom;
        done();
      })
      .catch(done);
    });
  });

  it('resets errors when a valid state is received', function(done) {
    const service = 'state/test';
    const flow = [
      { id: "disXmlState", type: "dim-xml-state-server",
        name: "DIM-XML State Server", cnf: "conf", serviceName: service,
        initState: "0", states: states },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dicXmlState = new DicXmlState(service, null, env.dns.url());

    helper.load([ DisXmlState, DimXmlConf ], flow, function() {
      const disXmlStateNode = helper.getNode("disXmlState");
      /* Check initial value */
      env.dicXmlState.then(
        (val) => expect(val).to.be.deep.equal(firstValue)
      )
      .then(async function() {
        const prom = waitForValue(env.dicXmlState, 'value', (val) => val,
          { ...firstValue, strValue: "ERROR", value: -2,
            errors: [
              { code: 500, message: "Server Error" },
              { code: 404, message: "Not Found" }
            ]
          });

        /* update value */
        disXmlStateNode.receive({
          payload: {
            errors: [
              { code: 500, message: "Server Error" },
              { code: 404, message: "Not Found" }
            ]
          }
        });
        await prom;
      })
      .then(async function() {
        const prom = waitForValue(env.dicXmlState, 'value', (val) => val,
          { ...firstValue, strValue: "Processing", value: 3, errors: [] });

        /* update value */
        disXmlStateNode.receive({ payload: { state: 3 } });
        await prom;

        done();
      })
      .catch(done);
    });
  });


  it('removes the service when distroyed', function(done) {
    const flow = [
      { id: "disXmlState", type: "dim-xml-state-server",
        name: "DIM-XML State Server", cnf: "conf", serviceName: "state/test",
        initState: "0", states: [ { value: "0", name: "idle" } ] },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DisXmlState, DimXmlConf ], flow, function() {
      const disXmlStateNode = helper.getNode("disXmlState");
      env.dis = disXmlStateNode.disXmlNode;

      try {
        expect(env.dis.isService('state/test')).to.be.true();
      }
      catch (err) {
        done(err);
      }

      helper.clearFlows() /* destroy the node */
      .then(() => {
        expect(env.dis.isService('state/test')).to.be.false();
        done();
      })
      .catch(done);
    });
  });

  const errTests = [
    /* node start-up phase (no inputs) */
    { name: "the initial state is not an integer", initState: "FIRST",
      states: [ { value: "0", name: "Initialization" },
        { value: "1", name: "Idle" } ],
      expectedErr: "disXmlState.error.set-state" },
    { name: "some value in the state list is not an integer", initState: "0",
      states: [ { value: "0", name: "Initialization" },
        { value: "FIRST", name: "Idle" } ],
      expectedErr: "disXmlState.error.add-state" },
    /* node operational phase */
    { name: "the input state is not an integer", initState: "0",
      states: [ { value: "0", name: "Initialization" },
        { value: "1", name: "Idle" } ],
      inputValue: { state: "FIRST" },
      expectedErr: "disXmlState.error.set-state" },
    { name: "the input error code is not an integer", initState: "0",
      states: [ { value: "0", name: "Initialization" },
        { value: "1", name: "Idle" } ],
      inputValue: { error: { code: "FIRST", message: "error msg" } },
      expectedErr: "disXmlState.error.add-error" },
    { name: "the input warning code is not an integer", initState: "0",
      states: [ { value: "0", name: "Initialization" },
        { value: "1", name: "Idle" } ],
      inputValue: { warning: { code: "FIRST", message: "warning msg" } },
      expectedErr: "disXmlState.error.add-warning" },
    { name: "some code in the input error list is not an integer",
      initState: "0", states: [ { value: "0", name: "Initialization" },
        { value: "1", name: "Idle" } ],
      inputValue: { errors: [ { code: "FIRST", message: "error msg" } ] },
      expectedErr: "disXmlState.error.add-error" },
    { name: "some code in the input warning list is not an integer",
      initState: "0", states: [ { value: "0", name: "Initialization" },
        { value: "1", name: "Idle" } ],
      inputValue: { warnings: [ { code: "FIRST", message: "warning msg" } ] },
      expectedErr: "disXmlState.error.add-warning" }
  ];

  describe('raises an error if', function() {
    errTests.forEach((t) => {
      it(`${t.name}`, function(done) {
        const flow = [
          { id: "disXmlState", type: "dim-xml-state-server",
            name: "DIM-XML State Server", cnf: "conf", serviceName: "state/test",
            initState: t.initState, states: t.states },
          { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
            retry: "1000" }
        ];

        helper.load([ DisXmlState, DimXmlConf ], flow, async function() {
          const disXmlStateNode = helper.getNode("disXmlState");

          disXmlStateNode.once('call:error', function(call) {
            try {
              expect(call.firstArg.toString()).to.be.deep.equal(t.expectedErr);

              done();
            }
            catch (err) {
              done(err);
            }
          });

          if (t.inputValue) {
            try {
              await waitEvent(disXmlStateNode.cnf.getDnsClient(), 'connect');
              expect(disXmlStateNode.status.lastCall.args).to.be.deep.equal(
                [ { fill: "green", shape: "dot",
                  text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" } ]);
              disXmlStateNode.receive({ payload: t.inputValue });
            }
            catch (err) {
              done(err);
            }
          }
        });
      });
    });
  });

});
