const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { waitEvent } = require('dim/test/utils'),
  { invoke } = require('lodash'),
  { delay } = require('prom'),

  { DnsServer, DnsClient } = require('dim'),
  { DisXmlNode, XmlCmd, xml } = require('dim-xml'),
  { DicXmlRpc, DimXmlConf } = require('../src');

describe('DicXmlRpc Node', function() {
  var env = {};

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    env.dis = new DisXmlNode();

    await env.dns.listen();
    const dnsClient = new DnsClient('localhost', 2505);
    await dnsClient.register(env.dis);
    dnsClient.unref();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dns, 'close');
    env = {};
  });

  it('can send an RPC request', function(done) {
    const flow = [
      { id: "dicXmlRpc", type: "dim-xml-rpc-client", name: "DIM-XML Rpc Client",
        cnf: "conf", serviceName: "rpc/test/success", timeout: "1000",
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    env.dis.addXmlRpc('rpc/test/success', (req) => {
      return "request received with key " + xml.toJs(req.xml).$.key;
    });

    helper.load([ DicXmlRpc, DimXmlConf ], flow, async function() {
      const dicXmlRpcNode = helper.getNode("dicXmlRpc");
      const helperNode = helper.getNode("help");

      helperNode.once('input', function(msg) {
        try {
          expect(dicXmlRpcNode.send.callCount).to.be.equal(1);
          expect(msg).to.deep.contains({
            payload: { $textContent: 'request received with key ' + msg.key },
            status: XmlCmd.Status.OK,
            error: XmlCmd.ERROR_NONE
          });
          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(dicXmlRpcNode.cnf.getDnsClient(), 'connect');
        expect(dicXmlRpcNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" } ]);
        dicXmlRpcNode.receive({ payload: { num: 123 } });
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can handle errors', function(done) {
    const flow = [
      { id: "dicXmlRpc", type: "dim-xml-rpc-client", name: "DIM-XML Rpc Client",
        cnf: "conf", serviceName: "rpc/test/error", timeout: "1000",
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    env.dis.addXmlRpc('rpc/test/error', () => {
      throw XmlCmd.createError(3, 'command failed');
    });

    helper.load([ DicXmlRpc, DimXmlConf ], flow, async function() {
      const dicXmlRpcNode = helper.getNode("dicXmlRpc");
      const helperNode = helper.getNode("help");

      helperNode.once('input', function(msg) {
        try {
          expect(dicXmlRpcNode.send.callCount).to.be.equal(1);
          expect(msg).to.deep.contains({
            payload: 'command failed',
            status: XmlCmd.Status.ERROR,
            error: 3
          });
          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(dicXmlRpcNode.cnf.getDnsClient(), 'connect');
        dicXmlRpcNode.receive({ payload: { num: 123 } });
      }
      catch (err) {
        done(err);
      }
    });
  });

  it(`raises an error if RPC service doesn't exist`, function(done) {
    const srvName = "test";
    const flow = [
      { id: "dicXmlRpc", type: "dim-xml-rpc-client", name: "DIM-XML Rpc Client",
        cnf: "conf", serviceName: srvName, timeout: "1000" },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DicXmlRpc, DimXmlConf ], flow, async function() {
      const dicXmlRpcNode = helper.getNode("dicXmlRpc");

      dicXmlRpcNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: service not found ' + srvName + '/RpcIn');

          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(dicXmlRpcNode.cnf.getDnsClient(), 'connect');
        expect(dicXmlRpcNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" } ]);
        dicXmlRpcNode.receive({ payload: { num: 123 } });
      }
      catch (err) {
        done(err);
      }
    });
  });

  it(`raises an error if reply isn't received before timeout`, function(done) {
    const flow = [
      { id: "dicXmlRpc", type: "dim-xml-rpc-client", name: "DIM-XML Rpc Client",
        cnf: "conf", serviceName: "rpc/test/delayed", timeout: "1000" },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dis.addXmlRpc('rpc/test/delayed', async (req) => {
      await delay(1500); /* ms */
      return "request received with key " + xml.toJs(req.xml).$.key;
    });

    helper.load([ DicXmlRpc, DimXmlConf ], flow, function() {
      const dicXmlRpcNode = helper.getNode("dicXmlRpc");

      dicXmlRpcNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: request time-out');

          done();
        }
        catch (err) {
          done(err);
        }
      });

      dicXmlRpcNode.receive({ payload: { num: 123 } });
    });
  });
});
