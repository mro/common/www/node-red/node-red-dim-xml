const
  helper = require('node-red-node-test-helper'),
  chai = require('chai'),
  dirtyChai = require('dirty-chai');

chai.use(dirtyChai);

helper.init(require.resolve('node-red'));
