const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach, before, after } = require('mocha'),

  { invoke, noop } = require('lodash'),
  { waitEvent } = require('dim/test/utils'),
  { DisXmlNode } = require('dim-xml'),
  { DnsServer, ServiceInfo, ServiceDefinition,
    DnsClient, DicCmd, DicDns } = require('dim'),
  { DimXmlConf } = require('../src');


describe('DimXmlConf Node', function() {
  var env = {};

  before(function(done) {
    helper.startServer(done);
  });

  after(function(done) {
    helper.stopServer(done);
  });

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    await env.dns.listen();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dns, 'close');
    env = {};
  });

  it('is loaded correctly', function(done) {
    const flow = [ { id: "conf", type: "dim-xml-conf", host: "localhost",
      port: "1234", retry: "5678" } ];

    helper.load(DimXmlConf, flow, function() {
      const confNode = helper.getNode("conf");

      try {
        expect(confNode).to.have.property('_host', 'localhost');
        expect(confNode).to.have.property('_port', '1234');
        expect(confNode).to.have.property('_retry', '5678');

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can query a service', function(done) {
    const flow = [ { id: "conf", type: "dim-xml-conf", host: "localhost",
      port: "2505" } ];

    helper.load(DimXmlConf, flow, async function() {
      const confNode = helper.getNode("conf");
      const dnsClient = confNode.getDnsClient();

      try {
        /* query a DNS Server built-in service */
        const srv = await dnsClient.query('DIS_DNS/SERVER_LIST');
        expect(srv).to.be.instanceOf(ServiceInfo);
        expect(srv.definition).to.be.deep.equal(ServiceDefinition.parse('C'));

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can spawn a DIM XML Service Provider', function(done) {
    const flow = [ { id: "conf", type: "dim-xml-conf", host: "localhost",
      port: "2505" } ];

    helper.load(DimXmlConf, flow, async function() {
      const confNode = helper.getNode("conf");

      try {
        const disXmlNode = confNode.getDisXmlNode(); /* spawn a Dis XML Node */
        disXmlNode.addXmlCmd('TEST', noop);
        expect(disXmlNode).to.be.instanceOf(DisXmlNode);

        const info = await DicDns.serviceInfo('TEST*', env.dns.url());

        expect(info).to.have.property("TEST/Cmd");
        expect(info).to.have.property("TEST/Ack");

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('automatically reconnects on Dns Server disconnection',
    function(done) {
      const flow = [ { id: "conf", type: "dim-xml-conf", host: "localhost",
        port: "2505", retry: "500" } ];

      helper.load(DimXmlConf, flow, async function() {
        const confNode = helper.getNode("conf");
        const dnsClient = confNode.getDnsClient();

        try {
          var prom = waitEvent(dnsClient, 'connect');
          dnsClient.connect();
          await prom;
          expect(confNode._connected).to.be.true();

          prom = waitEvent(dnsClient, 'disconnect');
          env.dns.close();
          await prom;
          expect(confNode._connected).to.be.false();

          prom = waitEvent(dnsClient, 'connect', 1, 1500);
          env.dns.listen();
          await prom;
          expect(confNode._connected).to.be.true();

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

  it('raises an error if the Dns Configuration is wrong', function(done) {
    const flow = [ { id: "conf", type: "dim-xml-conf", host: "localhost",
      port: "1234" } ];

    helper.load(DimXmlConf, flow, function() {
      const confNode = helper.getNode("conf");
      confNode.getDisXmlNode();

      confNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: failed to connect');

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });
  });

  it('releases resources once no longer needed', function(done) {
    const flow = [ { id: "conf", type: "dim-xml-conf", host: "localhost",
      port: "2505", retry: "1000" } ];

    helper.load(DimXmlConf, flow, async function() {
      const confNode = helper.getNode("conf");

      try {
        expect(confNode._dnsClient).to.be.null();
        expect(confNode._disXmlNode).to.be.null();

        confNode.getDisXmlNode();
        await waitEvent(confNode._dnsClient, 'connect');
        confNode.ref();

        expect(confNode._dnsClient).to.be.instanceOf(DnsClient);
        expect(confNode._disXmlNode).to.be.instanceOf(DisXmlNode);

        const prom = waitEvent(confNode._dnsClient, 'close');
        confNode.unref();
        await prom;

        expect(confNode._dnsClient).to.be.null();
        expect(confNode._disXmlNode).to.be.null();

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises a warning upon the DIS_DNS/KILL_SERVERS', function(done) {
    const flow = [ { id: "conf", type: "dim-xml-conf", host: "localhost",
      port: "2505", retry: "1000" } ];

    helper.load(DimXmlConf, flow, async function() {
      const confNode = helper.getNode("conf");

      confNode.once('call:warn', function(call) {
        try {
          expect(call.args.toString()).to.be.deep
          .equal('dimXmlConf.error.exit-command');
          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        const prom = waitEvent(confNode.getDnsClient(), 'connect');
        confNode.getDisXmlNode();
        await prom;

        DicCmd.invoke('DIS_DNS/KILL_SERVERS', null, env.dns.url());
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can retrieve the service list from a DNS server', function(done) {
    const flow = [ { id: "conf", type: "dim-xml-conf", host: "localhost",
      port: "2505", retry: "1000" } ];

    helper.load(DimXmlConf, flow, function() {
      const confNode = helper.getNode("conf");


      helper.request()
      .get('/service-list/' + confNode._host + ':' + confNode._port)
      .expect(200)
      .then((res) => {
        expect(res.body).to.haveOwnProperty('list');
        expect(res.body.list).to.have.deep.members(
          /* DNS Built-in Services */
          [
            { name: 'DIS_DNS/SERVICE_INFO', definition: 'C,C', type: 'RPC' },
            { name: 'DIS_DNS/KILL_SERVERS', definition: 'I', type: 'CMD' },
            { name: 'DIS_DNS/SERVER_LIST', definition: 'C', type: 'SRV' }
          ]
        );
        done();
      })
      .catch(done);
    });
  });
});
