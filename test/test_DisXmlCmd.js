const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { waitEvent } = require('dim/test/utils'),
  { invoke } = require('lodash'),

  { DnsServer } = require('dim'),
  { DicXmlCmd, XmlCmd } = require('dim-xml'),
  { DisXmlCmd, DimXmlConf } = require('../src');

describe('DisXmlCmd Nodes', function() {

  var env = {};
  const nodes = [
    { name: "DIM-XML Cmd Server", type: "dim-xml-cmd-server" },
    { name: "DIM-XML Cmd IN", type: "dim-xml-cmd in" }
  ];

  const cmdParams = [
    { type: 'string', sent: 'TEST', received: { $textContent: 'TEST' } },
    { type: 'number', sent: 1234, received: { $textContent: '1234' } },
    { type: 'boolean', sent: false, received: { $textContent: 'false' } },
    {
      type: 'JS object',
      sent: { num: 1.23, str: 'test', arr: [ 4, 5, 6 ], bool: true,
        obj: { p: 'test' }
      },
      received: { num: '1.23', str: 'test', arr: [ '4', '5', '6' ],
        bool: 'true', obj: { p: 'test' }
      }
    },
    {
      type: 'JS object with text content',
      sent: { num: 1.23, $textContent: "command text content" },
      received: { num: '1.23', $textContent: "command text content" }
    }
  ];

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    await env.dns.listen();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dns, 'close');
    env = {};
  });

  nodes.forEach((n) => {
    cmdParams.forEach((p) => {

      it(`[ ${n.name} ] can receive a command (param: ${p.type})` +
        (n.type === 'dim-xml-cmd-server' ? ` and acknowledge it` : ''),
      function(done) {
        const flow = [
          { id: "disXmlCmd", type: `${n.type}`,
            name: `${n.name}`, cnf: "conf", serviceName: "cmd/test",
            wires: [ [ "help" ] ] },
          { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
            retry: "1000" },
          { id: "help", type: "helper" }
        ];

        helper.load([ DisXmlCmd, DimXmlConf ], flow, async function() {
          const disXmlCmdNode = helper.getNode("disXmlCmd");
          const helperNode = helper.getNode("help");

          helperNode.once('input', function(msg) {
            try {
              expect(disXmlCmdNode.send.callCount).to.be.equal(1);
              expect(msg.payload).to.be.deep.equal(p.received);
            }
            catch (err) {
              done(err);
            }
          });

          try {
            await waitEvent(disXmlCmdNode.cnf.getDnsClient(), 'connect');
            expect(disXmlCmdNode.status.lastCall.args).to.be.deep.equal(
              [ { fill: "green", shape: "dot",
                text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" } ]);

            DicXmlCmd.invoke('cmd/test', p.sent, env.dns.url())
            .then(
              (ack) => {
                expect(n.type).to.be.equal('dim-xml-cmd-server');
                expect(ack.status).to.be.equal(XmlCmd.Status.OK);
                expect(ack.error).to.be.equal(XmlCmd.ERROR_NONE);
                done();
              },
              (err) => {
                expect(n.type).to.be.equal('dim-xml-cmd in');
                expect(err.toString()).to.be.equal('Error: request time-out');
                done();
              }
            ).catch(done);

          }
          catch (err) {
            done(err);
          }
        });
      });
    });

    it(`[ ${n.name} ] removes the service when distroyed`, function(done) {
      const flow = [
        { id: "disXmlCmd", type: `${n.type}`,
          name: `${n.name}`, cnf: "conf", serviceName: "cmd/test" },
        { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
          retry: "1000" }
      ];

      helper.load([ DisXmlCmd, DimXmlConf ], flow, async function() {
        const disXmlCmdNode = helper.getNode("disXmlCmd");
        env.dis = disXmlCmdNode.disXmlNode;
        try {
          await waitEvent(disXmlCmdNode.cnf.getDnsClient(), 'connect');

          expect(env.dis.isService('cmd/test/Cmd')).to.be.true();
        }
        catch (err) {
          done(err);
        }

        helper.clearFlows() /* destroy the node */
        .then(() => {
          expect(env.dis.isService('cmd/test/Cmd')).to.be.false();
          done();
        })
        .catch(done);
      });
    });
  });

  it('[ DIM-XML Cmd Response ] can serve a command', function(done) {
    const flow = [
      { id: "dimXmlCmdIn", type: "dim-xml-cmd in", name: "DIM-XML Cmd IN",
        cnf: "conf", serviceName: "cmd/test",
        wires: [ [ "dimXmlCmdRes" ] ] },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "dimXmlCmdRes", type: "dim-xml-cmd-response" }
    ];

    helper.load([ DisXmlCmd, DimXmlConf ], flow, async function() {
      const dimXmlCmdInNode = helper.getNode('dimXmlCmdIn');
      const dimXmlCmdResNode = helper.getNode("dimXmlCmdRes");

      dimXmlCmdResNode.once('call:debug', function(call) {
        try {
          expect(call.args).to.be.deep.equal(
            [ 'disXmlCmd.success.cmd-served' ]);

          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(dimXmlCmdInNode.cnf.getDnsClient(), 'connect');
        DicXmlCmd.invoke('cmd/test', { num: 123 }, env.dns.url());
      }
      catch (err) {
        done(err);
      }
    });
  });

  it(`[ DIM-XML Cmd Response ] raises an error if the input message doesn't` +
    ` come from a 'DIM-XML Cmd IN' node`, function(done) {
    const flow = [ { id: "dimXmlCmdRes", type: "dim-xml-cmd-response" } ];

    helper.load(DisXmlCmd, flow, function() {
      const dimXmlCmdResNode = helper.getNode("dimXmlCmdRes");

      dimXmlCmdResNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: disXmlCmd.error.cmd-failed');

          done();
        }
        catch (err) {
          done(err);
        }
      });

      const msg = { payload: '1234' };
      dimXmlCmdResNode.receive(msg);
    });
  });
});
