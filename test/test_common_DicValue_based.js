/* eslint-disable max-lines */
const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { waitEvent } = require('dim/test/utils'),
  { waitForValue } = require('./utils'),
  { invoke, now } = require('lodash'),
  { delay } = require('prom'),

  { DnsServer, DnsClient } = require('dim'),
  { DisXmlNode, DisXmlState, DisXmlDataSet, DisXmlParams } = require('dim-xml'),
  { DicXmlState, DicXmlDataSet, DicXmlParams, DimXmlConf } = require('../src');

describe('DicValue-based Nodes (State, DataSet, Params)', function() {
  var env = {};
  const nodes = [
    { toTest: DicXmlState, name: "DIM-XML State Client",
      type: "dim-xml-state-client", serviceName: "state/test",
      addService: DisXmlNode.prototype.addXmlState,
      setValue: DisXmlState.prototype.setState,
      initService: (srv) => {
        srv.addState(0, 'Initialization');
        srv.addState(1, 'Idle');
        srv.addState(2, 'Processing');
        srv.setState(0);
      },
      initValue: {
        description: [
          { strValue: "ERROR", value: -2 },
          { strValue: "NOT READY", value: -1 },
          { strValue: "Initialization", value: 0 },
          { strValue: "Idle", value: 1 },
          { strValue: "Processing", value: 2 }
        ],
        strValue: "Initialization",
        value: 0,
        errors: [],
        warnings: []
      },
      updateValue: 1,
      expectedValue: {
        description: [
          { strValue: "ERROR", value: -2 },
          { strValue: "NOT READY", value: -1 },
          { strValue: "Initialization", value: 0 },
          { strValue: "Idle", value: 1 },
          { strValue: "Processing", value: 2 }
        ],
        strValue: "Idle",
        value: 1,
        errors: [],
        warnings: []
      }
    },
    { toTest: DicXmlDataSet, name: "DIM-XML DataSet Client",
      type: "dim-xml-dataset-client", serviceName: "dataset/test",
      addService: DisXmlNode.prototype.addXmlDataSet,
      setValue: DisXmlDataSet.prototype.update,
      initService: (srv) => {
        srv.add({ index: 0, name: 'sampleRate', unit: 'MS/s', type: 8,
          value: 1500 });
        srv.add({ index: 1, name: 'sampleSize', unit: 'kS', type: 11,
          value: 7000 });
        srv.add({ index: 2, name: 'active', unit: '', type: 9, value: true });
      },
      initValue: [
        { index: 0, name: 'sampleRate', unit: 'MS/s', type: 8, value: 1500 },
        { index: 1, name: 'sampleSize', unit: 'kS', type: 11, value: 7000 },
        { index: 2, name: 'active', type: 9, value: true }
      ],
      updateValue: { index: 0, value: 1000 },
      expectedValue: [
        { index: 0, name: 'sampleRate', unit: 'MS/s', type: 8, value: 1000 },
        { index: 1, name: 'sampleSize', unit: 'kS', type: 11, value: 7000 },
        { index: 2, name: 'active', type: 9, value: true }
      ]
    },
    { toTest: DicXmlParams, name: "DIM-XML Params Client",
      type: "dim-xml-params-client", serviceName: "params/test",
      addService: DisXmlNode.prototype.addXmlParams,
      setValue: DisXmlParams.prototype.update,
      initService: (srv) => {
        srv.add({ index: 0, name: 'sampleRate', unit: 'MS/s', type: 8,
          value: 1500 });
        srv.add({ index: 1, name: 'sampleSize', unit: 'kS', type: 11,
          value: 7000 });
        srv.add({ index: 2, name: 'active', unit: '', type: 9, value: true });
      },
      initValue: [
        { index: 0, name: 'sampleRate', unit: 'MS/s', type: 8, value: 1500 },
        { index: 1, name: 'sampleSize', unit: 'kS', type: 11, value: 7000 },
        { index: 2, name: 'active', type: 9, value: true }
      ],
      updateValue: { index: 0, value: 1000 },
      expectedValue: [
        { index: 0, name: 'sampleRate', unit: 'MS/s', type: 8, value: 1000 },
        { index: 1, name: 'sampleSize', unit: 'kS', type: 11, value: 7000 },
        { index: 2, name: 'active', type: 9, value: true }
      ]
    }
  ];

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    env.dis = new DisXmlNode();

    await env.dns.listen();
    const dnsClient = new DnsClient('localhost', 2505);
    await dnsClient.register(env.dis);
    dnsClient.unref();
    env.v = []; /* to collect values */
    env.ts = []; /* to collect timestamps */
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dns, 'close');
    env = {};
  });

  nodes.forEach((n) => {
    it(`[ ${n.name} ] can monitor a value`, function(done) {
      const flow = [
        { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
          serviceName: `${n.serviceName}`, timeout: "5000",
          monitored: true, scheduled: false, stamped: false,
          wires: [ [ "help" ] ] },
        { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
          retry: "1000" },
        { id: "help", type: "helper" }
      ];

      const srv = n.addService.call(env.dis, n.serviceName);
      n.initService(srv);

      helper.load([ n.toTest, DimXmlConf ], flow, async function() {
        const node = helper.getNode("node");
        const helperNode = helper.getNode("help");

        try {
          /* wait for the initial value */
          await waitForValue(helperNode, 'input', (msg) => {
            env.ts.push(now());
            return msg.payload;
          }, n.initValue);

          await delay(400); /* ms */

          const prom = waitForValue(helperNode, 'input', (msg) => {
            env.ts.push(now());
            return msg.payload;
          }, n.expectedValue);
          n.setValue.call(srv, n.updateValue);
          await prom;

          expect(node.send.callCount).to.be.equal(2);
          expect(env.ts[1] - env.ts[0]).to.be.within(350, 450);

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it(`[ ${n.name} ] can poll a value`, function(done) {
      const flow = [
        { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
          serviceName: `${n.serviceName}`, timeout: "1000",
          monitored: false, scheduled: true, stamped: false,
          wires: [ [ "help" ] ] },
        { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
          retry: "1000" },
        { id: "help", type: "helper" }
      ];

      const srv = n.addService.call(env.dis, n.serviceName);
      n.initService(srv);

      helper.load([ n.toTest, DimXmlConf ], flow, async function() {
        const node = helper.getNode("node");
        const helperNode = helper.getNode("help");

        try {
          /* wait for the initial value */
          await waitForValue(helperNode, 'input', (msg) => {
            env.ts.push(now());
            n.setValue.call(srv, n.updateValue);
            return msg.payload;
          }, n.initValue);

          await waitForValue(helperNode, 'input', (msg) => {
            env.ts.push(now());
            return msg.payload;
          }, n.expectedValue, 2000);

          expect(node.send.callCount).to.be.equal(2);
          expect(env.ts[1] - env.ts[0]).to.be.within(950, 1050);

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it(`[ ${n.name} ] can retrieve a timestamped value`, function(done) {
      const flow = [
        { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
          serviceName: `${n.serviceName}`, timeout: "5000",
          monitored: true, scheduled: false, stamped: true,
          wires: [ [ "help" ] ] },
        { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
          retry: "1000" },
        { id: "help", type: "helper" }
      ];

      const srv = n.addService.call(env.dis, n.serviceName);
      n.initService(srv);

      helper.load([ n.toTest, DimXmlConf ], flow, function() {
        const helperNode = helper.getNode("help");

        helperNode.once('input', function(msg) {
          try {
            expect(msg.payload).to.be.deep.equal(n.initValue);
            expect(msg).to.have.property('timestamp');
            expect(msg.timestamp).to.be.closeTo(now(), 1000);
            done();
          }
          catch (err) {
            done(err);
          }
        });
      });
    });

    it(`[ ${n.name} ] can monitor a value with timeout`, function(done) {
      const flow = [
        { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
          serviceName: `${n.serviceName}`, timeout: "1000",
          monitored: true, scheduled: true, stamped: true,
          wires: [ [ "help" ] ] },
        { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
          retry: "1000" },
        { id: "help", type: "helper" }
      ];

      const srv = n.addService.call(env.dis, n.serviceName);
      n.initService(srv);

      helper.load([ n.toTest, DimXmlConf ], flow, async function() {
        const node = helper.getNode("node");
        const helperNode = helper.getNode("help");

        /* wait for 3 value:
          1- initial value (~ 0 ms)
          2- value updated (~ 200 ms)
          3- timeout expired (~ 1000 ms)
        */

        try {
          await waitForValue(helperNode, 'input', (msg) => {
            env.ts.push(now());
            return msg.payload;
          }, n.initValue);

          await delay(200); /* ms */

          const prom = waitForValue(helperNode, 'input', (msg) => {
            env.ts.push(now());
            return msg.payload;
          }, n.expectedValue);
          n.setValue.call(srv, n.updateValue);
          await prom;

          await waitForValue(helperNode, 'input', (msg) => {
            env.ts.push(now());
            return msg.payload;
          }, n.expectedValue);

          expect(node.send.callCount).to.be.equal(3);
          /* Interarrival time after update */
          expect(env.ts[1] - env.ts[0]).to.be.within(150, 250);
          /* Interarrival time after timeout */
          expect(env.ts[2] - env.ts[1]).to.be.within(750, 850);

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it(`[ ${n.name} ] can detect disconnections (monitored mode)`,
      function(done) {
        const flow = [
          { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
            serviceName: `${n.serviceName}`, timeout: "1000",
            monitored: true, scheduled: false, stamped: false,
            wires: [ [ "help" ] ] },
          { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
            retry: "1000" },
          { id: "help", type: "helper" }
        ];

        const srv = n.addService.call(env.dis, n.serviceName);
        n.initService(srv);

        helper.load([ n.toTest, DimXmlConf ], flow, async function() {
          const helperNode = helper.getNode("help");

          try {
            await waitForValue(helperNode, 'input', (msg) => msg.payload,
              n.initValue);
            env.dis.close();
            await waitForValue(helperNode, 'input', (msg) => msg.payload,
              undefined);

            done();
          }
          catch (err) {
            done(err);
          }
        });
      });

    it(`[ ${n.name} ] can detect disconnections (scheduled mode)`,
      function(done) {
        const flow = [
          { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
            serviceName: `${n.serviceName}`, timeout: "1000",
            monitored: false, scheduled: true, stamped: false,
            wires: [ [ "help" ] ] },
          { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
            retry: "1000" },
          { id: "help", type: "helper" }
        ];

        const srv = n.addService.call(env.dis, n.serviceName);
        n.initService(srv);

        helper.load([ n.toTest, DimXmlConf ], flow, async function() {
          const helperNode = helper.getNode("help");

          try {
            await waitForValue(helperNode, 'input', (msg) => msg.payload,
              n.initValue);
            env.dis.close();
            await waitForValue(helperNode, 'input', (msg) => msg.payload,
              undefined);

            done();
          }
          catch (err) {
            done(err);
          }
        });
      });

    it(`[ ${n.name} ] can reconnect a service`, function(done) {
      const flow = [
        { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
          serviceName: `${n.serviceName}`, timeout: "1000",
          monitored: false, scheduled: true, stamped: false,
          wires: [ [ "help" ] ] },
        { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
          retry: "1000" },
        { id: "help", type: "helper" }
      ];

      let srv = n.addService.call(env.dis, n.serviceName);
      n.initService(srv);

      helper.load([ n.toTest, DimXmlConf ], flow, async function() {
        const helperNode = helper.getNode("help");

        helperNode.on('input', function(msg) {
          env.v.push(msg.payload);
        });

        try {
          await waitForValue(helperNode, 'input', (msg) => msg.payload,
            n.initValue);

          const prom = waitEvent(env.dis, 'close');
          env.dis.close();
          await prom;

          /* restore the service */
          env.dis = new DisXmlNode();
          srv = n.addService.call(env.dis, n.serviceName);
          n.initService(srv);
          const dnsClient = new DnsClient('localhost', 2505);
          await dnsClient.register(env.dis);
          dnsClient.unref();

          await waitForValue(helperNode, 'input', (msg) => msg.payload,
            n.initValue);

          expect(env.v).to.be.deep.equal([ n.initValue, undefined,
            n.initValue ]);

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it(`[ ${n.name} ] can retrieve a single value`, function(done) {
      const flow = [
        { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
          serviceName: `${n.serviceName}`, timeout: "1000",
          monitored: false, scheduled: false, stamped: false,
          wires: [ [ "help" ] ] },
        { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
          retry: "1000" },
        { id: "help", type: "helper" }
      ];

      const srv = n.addService.call(env.dis, n.serviceName);
      n.initService(srv);

      helper.load([ n.toTest, DimXmlConf ], flow, function() {
        const helperNode = helper.getNode("help");

        helperNode.on('input', function(msg) {
          try {
            expect(msg.payload).to.be.deep.equal(n.expectedValue);
            done();
          }
          catch (err) {
            done(err);
          }
        });

        n.setValue.call(srv, n.updateValue);

        helper.request().post(`/${n.type}/node`).expect(200)
        .catch(done);
      });
    });

    it(`[ ${n.name} ] raises an error if service does not exist`,
      function(done) {
        const flow = [
          { id: "node", type: `${n.type}`, name: `${n.name}`, cnf: "conf",
            serviceName: "INEXISTENT/STATE/SERVICE", timeout: "1000",
            monitored: false, scheduled: false, stamped: false },
          { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
            retry: "1000" }
        ];

        const srv = n.addService.call(env.dis, n.serviceName);
        n.initService(srv);

        helper.load([ n.toTest, DimXmlConf ], flow, function() {
          const node = helper.getNode("node");

          node.once('call:error', function(call) {
            try {
              expect(call.firstArg.toString())
              .to.be.deep.equal('Error: failed to parse document');

              done();
            }
            catch (err) {
              done(err);
            }
          });

          helper.request().post(`/${n.type}/node`).expect(200)
          .catch(done);
        });
      });
  });
});
