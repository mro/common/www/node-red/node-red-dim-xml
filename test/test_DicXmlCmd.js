const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { waitEvent } = require('dim/test/utils'),
  { invoke } = require('lodash'),
  { delay } = require('prom'),

  { DnsServer, DnsClient } = require('dim'),
  { DisXmlNode, XmlCmd, xml } = require('dim-xml'),
  { DicXmlCmd, DimXmlConf } = require('../src');

describe('DicXmlCmd Node', function() {
  var env = {};

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    env.dis = new DisXmlNode();

    await env.dns.listen();
    const dnsClient = new DnsClient('localhost', 2505);
    await dnsClient.register(env.dis);
    dnsClient.unref();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dns, 'close');
    env = {};
  });

  it('can send an acknowledged command', function(done) {
    const flow = [
      { id: "dicXmlCmd", type: "dim-xml-cmd-client", name: "DIM-XML Cmd Client",
        cnf: "conf", serviceName: "cmd/test/success", timeout: "1000",
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    env.dis.addXmlCmd('cmd/test/success', (req) => {
      return "command received with key " + xml.toJs(req.xml).$.key;
    });

    helper.load([ DicXmlCmd, DimXmlConf ], flow, async function() {
      const dicXmlCmdNode = helper.getNode("dicXmlCmd");
      const helperNode = helper.getNode("help");

      helperNode.once('input', function(msg) {
        try {
          expect(dicXmlCmdNode.send.callCount).to.be.equal(1);
          expect(msg).to.deep.contains({
            payload: 'command received with key ' + msg.key,
            status: XmlCmd.Status.OK,
            error: XmlCmd.ERROR_NONE
          });
          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(dicXmlCmdNode.cnf.getDnsClient(), 'connect');
        expect(dicXmlCmdNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" } ]);
        dicXmlCmdNode.receive({ payload: { num: 123 } });
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can handle errors', function(done) {
    const flow = [
      { id: "dicXmlCmd", type: "dim-xml-cmd-client", name: "DIM-XML Cmd Client",
        cnf: "conf", serviceName: "cmd/test/error", timeout: "1000",
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    env.dis.addXmlCmd('cmd/test/error', () => {
      throw XmlCmd.createError(3, 'command failed');
    });

    helper.load([ DicXmlCmd, DimXmlConf ], flow, async function() {
      const dicXmlCmdNode = helper.getNode("dicXmlCmd");
      const helperNode = helper.getNode("help");

      helperNode.once('input', function(msg) {
        try {
          expect(dicXmlCmdNode.send.callCount).to.be.equal(1);
          expect(msg).to.deep.contains({
            payload: 'command failed',
            status: XmlCmd.Status.ERROR,
            error: 3
          });
          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(dicXmlCmdNode.cnf.getDnsClient(), 'connect');
        dicXmlCmdNode.receive({ payload: { num: 123 } });
      }
      catch (err) {
        done(err);
      }
    });
  });

  it(`raises an error if CMD service doesn't exist`, function(done) {
    const srvName = "test";
    const flow = [
      { id: "dicXmlCmd", type: "dim-xml-cmd-client", name: "DIM-XML Cmd Client",
        cnf: "conf", serviceName: srvName, timeout: "1000" },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DicXmlCmd, DimXmlConf ], flow, async function() {
      const dicXmlCmdNode = helper.getNode("dicXmlCmd");

      dicXmlCmdNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: service not found ' + srvName + '/Cmd');

          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(dicXmlCmdNode.cnf.getDnsClient(), 'connect');
        expect(dicXmlCmdNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim-xml/dimXmlConf:dimXmlConf.status.connected" } ]);
        dicXmlCmdNode.receive({ payload: { num: 123 } });
      }
      catch (err) {
        done(err);
      }
    });
  });

  it(`raises an error if reply isn't received before timeout`, function(done) {
    const flow = [
      { id: "dicXmlCmd", type: "dim-xml-cmd-client", name: "DIM-XML Cmd Client",
        cnf: "conf", serviceName: "cmd/test/delayed", timeout: "1000" },
      { id: "conf", type: "dim-xml-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dis.addXmlCmd('cmd/test/delayed', async (req) => {
      await delay(1500); /* ms */
      return "command received with key " + xml.toJs(req.xml).$.key;
    });

    helper.load([ DicXmlCmd, DimXmlConf ], flow, function() {
      const dicXmlCmdNode = helper.getNode("dicXmlCmd");

      dicXmlCmdNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: request time-out');

          done();
        }
        catch (err) {
          done(err);
        }
      });

      dicXmlCmdNode.receive({ payload: { num: 123 } });
    });
  });
});
